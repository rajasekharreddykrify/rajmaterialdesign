package rajmaterialdemo.com.rajmaterialdemo.asterisk;

import junit.framework.TestCase;

import org.asteriskjava.manager.DefaultManagerConnection;
import org.asteriskjava.manager.ManagerEventListener;
import org.asteriskjava.manager.action.StatusAction;
import org.asteriskjava.manager.event.ManagerEvent;

/**
 * Created by raj on 29/2/16.
 */
public class TestDefaultManagerConnection extends TestCase
{
    private DefaultManagerConnection getDefaultManagerConnection()
    {
        DefaultManagerConnection dmc;

        dmc = new DefaultManagerConnection();
        dmc.setUsername("krify1");
        dmc.setPassword("123456");
        dmc.setHostname("209.190.64.11");

        return dmc;
    }

    public void testLogin() throws Exception
    {
        DefaultManagerConnection dmc;

        dmc = getDefaultManagerConnection();
        dmc.login();
        dmc.addEventListener(new ManagerEventListener() {
            @Override
            public void onManagerEvent(ManagerEvent managerEvent) {
                System.out.println(managerEvent);
            }
        });


//        dmc.addEventHandler(new ManagerEventHandler()
//        {
//            public void handleEvent(ManagerEvent event)
//            {
//                System.out.println(event);
//            }
//        });
        dmc.sendAction(new StatusAction());

// wait for 3 seconds to receive events
        Thread.sleep(3000);
        dmc.logoff();
    }
}