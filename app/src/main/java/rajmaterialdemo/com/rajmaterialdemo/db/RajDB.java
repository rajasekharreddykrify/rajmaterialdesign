package rajmaterialdemo.com.rajmaterialdemo.db;

/**
 * Created by raj on 29/10/15.
 */


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import rajmaterialdemo.com.rajmaterialdemo.user.UserDetails;
import rajmaterialdemo.com.rajmaterialdemo.utils.L;

public class RajDB {

    public Context ctx;
    SQLiteDatabase db;

    MyDatabase my_db;

    public RajDB(Context con) {
        this.ctx = con;
        my_db = new MyDatabase(con, DB_PARAMS.DATABASE_NAME, null, DB_PARAMS.DATABASE_VERSION);
        db = my_db.getReadableDatabase();
    }

    public void insertRecord(String id,String name, String email, String mobile, String platform,String imageurl) {

        SQLiteDatabase db = my_db.getWritableDatabase();


        ContentValues cv = new ContentValues();

        cv.put(DB_PARAMS.COLUMNS.details.ID, id);

        cv.put(DB_PARAMS.COLUMNS.details.NAME, name);
        cv.put(DB_PARAMS.COLUMNS.details.EMAIL, email);

        cv.put(DB_PARAMS.COLUMNS.details.MOBILE, mobile);
        cv.put(DB_PARAMS.COLUMNS.details.PLATFORM, platform);
        cv.put(DB_PARAMS.COLUMNS.details.IMAGEURL, imageurl);

        if (platform.equals("ANDROID")) {
            db.insert(DB_PARAMS.TABLE.ANDROID, null, cv);
        } else if (platform.equals("IOS")) {
            db.insert(DB_PARAMS.TABLE.IOS, null, cv);
        } else if (platform.equals("WINDOWS")) {
            db.insert(DB_PARAMS.TABLE.WINDOWS, null, cv);
        }


        db.close();


    }



    public void updateRecord(String id,String name, String email, String mobile, String platform,String imageurl) {

        L.i("updateRecord","updateRecord");

        SQLiteDatabase db = my_db.getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(DB_PARAMS.COLUMNS.details.ID, id);

        cv.put(DB_PARAMS.COLUMNS.details.NAME, name);
        cv.put(DB_PARAMS.COLUMNS.details.EMAIL, email);

        cv.put(DB_PARAMS.COLUMNS.details.MOBILE, mobile);
        cv.put(DB_PARAMS.COLUMNS.details.PLATFORM, platform);
        cv.put(DB_PARAMS.COLUMNS.details.IMAGEURL, imageurl);

        if (platform.equals("ANDROID")) {
            db.update(DB_PARAMS.TABLE.ANDROID, cv, DB_PARAMS.COLUMNS.details.ID + "=" + id, null);
        } else if (platform.equals("IOS")) {
            db.update(DB_PARAMS.TABLE.IOS, cv, DB_PARAMS.COLUMNS.details.ID + "=" + id, null);
        } else if (platform.equals("WINDOWS")) {
            db.update(DB_PARAMS.TABLE.WINDOWS, cv, DB_PARAMS.COLUMNS.details.ID + "=" + id, null);
        }


        db.close();


    }

    public int getProfilesCount(String TABLE_NAME) {
        String countQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = my_db.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }

    public int deleteAllAndroid(){
        return db.delete(DB_PARAMS.TABLE.ANDROID, null, null);
    }

    public void delete_byID(String Table_name,int id){

        if(Table_name.equals(DB_PARAMS.TABLE.ANDROID)){
            db.delete(DB_PARAMS.TABLE.ANDROID, DB_PARAMS.COLUMNS.details.ID + "=" + id, null);
        }else if(Table_name.equals(DB_PARAMS.TABLE.WINDOWS)){
            db.delete(DB_PARAMS.TABLE.WINDOWS, DB_PARAMS.COLUMNS.details.ID + "=" + id, null);

        }else if(Table_name.equals(DB_PARAMS.TABLE.IOS)){
            db.delete(DB_PARAMS.TABLE.IOS, DB_PARAMS.COLUMNS.details.ID + "=" + id, null);

        }

    }

    public void delete_byEmail(String Table_name,String email){

        if(Table_name.equals(DB_PARAMS.TABLE.ANDROID)){
            db.delete(DB_PARAMS.TABLE.ANDROID, DB_PARAMS.COLUMNS.details.EMAIL + "='" + email+"'", null);
        }else if(Table_name.equals(DB_PARAMS.TABLE.WINDOWS)){
            db.delete(DB_PARAMS.TABLE.WINDOWS, DB_PARAMS.COLUMNS.details.EMAIL + "='" + email+"'", null);

        }else if(Table_name.equals(DB_PARAMS.TABLE.IOS)){
            db.delete(DB_PARAMS.TABLE.IOS, DB_PARAMS.COLUMNS.details.EMAIL + "='" + email+"'", null);

        }

    }

    public boolean checkEmailAlreadyExistsOrNot(String email,String team){

        boolean flag = false;

        String query = "";
        if(team.equals("ANDROID")){
              query = "SELECT * FROM "+ DB_PARAMS.TABLE.ANDROID + " WHERE "
                    + DB_PARAMS.COLUMNS.details.EMAIL+" ='"+email+"'";
        }else  if(team.equals("IOS")){
              query = "SELECT * FROM "+ DB_PARAMS.TABLE.IOS + " WHERE "
                    + DB_PARAMS.COLUMNS.details.EMAIL+" ='"+email+"'";
        }else  if(team.equals("WINDOWS")){
              query = "SELECT * FROM "+ DB_PARAMS.TABLE.WINDOWS + " WHERE "
                    + DB_PARAMS.COLUMNS.details.EMAIL+" ='"+email+"'";
        }

        L.i("DB", "checkEmailAlreadyExistsOrNot query is : " + query);

        SQLiteDatabase db = my_db.getReadableDatabase();
        Cursor cursor = db.rawQuery(query,null);



        if(cursor.getCount()>0){
            flag =true;
            L.i("DB","checkEmailAlreadyExistsOrNot query result is : Email already Exists");
        }

        return  flag;
    }

    public boolean checkPhoneNumberAlreadyExistsOrNot(String phone_number,String team){

        boolean flag = false;

        String query = "";
        if(team.equals("ANDROID")){
            query = "SELECT * FROM "+ DB_PARAMS.TABLE.ANDROID + " WHERE "
                    + DB_PARAMS.COLUMNS.details.MOBILE+" ='"+phone_number+"'";
        }else  if(team.equals("IOS")){
            query = "SELECT * FROM "+ DB_PARAMS.TABLE.IOS + " WHERE "
                    + DB_PARAMS.COLUMNS.details.MOBILE+" ='"+phone_number+"'";
        }else  if(team.equals("WINDOWS")){
            query = "SELECT * FROM "+ DB_PARAMS.TABLE.WINDOWS + " WHERE "
                    + DB_PARAMS.COLUMNS.details.MOBILE+" ='"+phone_number+"'";
        }

        L.i("DB","checkPhoneNumberAlreadyExistsOrNot query is : "+query);

        SQLiteDatabase db = my_db.getReadableDatabase();
        Cursor cursor = db.rawQuery(query,null);



        if(cursor.getCount()>0){
            flag =true;
            L.i("DB","checkPhoneNumberAlreadyExistsOrNot query result is : Phone Number already Exists");
        }

        return  flag;
    }

    //
//
//    public void deleteCricketerRecord(String name,String team_name,String specialist){
//
//        SQLiteDatabase db = my_db.getWritableDatabase();
//
//        ContentValues cv = new ContentValues();
//
//        cv.put(DB_PARAMS.COLUMNS.cricket.NAME, name);
//
//        cv.put(DB_PARAMS.COLUMNS.cricket.TEAM_NAME, team_name);
//
//        cv.put(DB_PARAMS.COLUMNS.cricket.SPECIALIST, specialist);
//
//        db.delete(DB_PARAMS.TABLE.CRICKET, DB_PARAMS.COLUMNS.cricket.NAME +"='"+name, null);
//
//        db.close();
//
//
//
//    }
//
    public Cursor getAndroidRecords() {

        Cursor c = null;

        try {
            SQLiteDatabase db = my_db.getReadableDatabase();

            c = db.rawQuery("SELECT * FROM " + DB_PARAMS.TABLE.ANDROID, null);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return c;

    }

    public UserDetails getAndroidRecordUsingEmail(String table_name,String email) {

        Cursor c = null;
        String query = "";

        UserDetails ud = new UserDetails();

        try {
            SQLiteDatabase db = my_db.getReadableDatabase();

            query = "SELECT * FROM " + DB_PARAMS.TABLE.ANDROID + " Where "
                    + DB_PARAMS.COLUMNS.details.EMAIL +"='"+ email+"'";



            L.i("getAndroidRecordUsingEmail", "query is : " + query);
            c = db.rawQuery(query, null);


            if(c.moveToFirst()){
                ud.setId(c.getString(1));
                ud.setName(c.getString(2));
                ud.setEmail(c.getString(3));
                ud.setMobile(c.getString(4));
                ud.setPlatform(c.getString(5));
                ud.setImageurl(c.getString(6));
            }




        } catch (Exception e) {
            e.printStackTrace();
        }


        return ud;

    }

    public Cursor getCursorAndroidRecordUsingEmail(String table_name,String email) {

        Cursor c = null;
        String query = "";


        try {
            SQLiteDatabase db = my_db.getReadableDatabase();

            query = "SELECT * FROM " + DB_PARAMS.TABLE.ANDROID + " Where "
                    + DB_PARAMS.COLUMNS.details.EMAIL +"='"+ email+"'";



            L.i("getAndroidRecordUsingEmail", "query is : " + query);
            c = db.rawQuery(query, null);

        } catch (Exception e) {
            e.printStackTrace();
        }


        return c;

    }


    public Cursor getAndroidRecordUsingId(String table_name,String id) {

        Cursor c = null;
        String query = "";


        try {
            SQLiteDatabase db = my_db.getReadableDatabase();

            query = "SELECT * FROM " + DB_PARAMS.TABLE.ANDROID + " Where "
                    + DB_PARAMS.COLUMNS.details.ID +"='"+ id+"'";



            L.i("getAndroidRecordUsingId", "query is : " + query);
            c = db.rawQuery(query, null);

        } catch (Exception e) {
            e.printStackTrace();
        }


        return c;

    }
    public class MyDatabase extends SQLiteOpenHelper {

        //factory to use for creating cursor objects, or null for the default

        public MyDatabase(Context context, String name, CursorFactory factory,
                          int version) {
            super(context, DB_PARAMS.DATABASE_NAME, null, DB_PARAMS.DATABASE_VERSION);
            // TODO Auto-generated constructor stub


        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // TODO Auto-generated method stub

            db.execSQL(DB_QUERIES.CREATE_TABLE.ANDROID_TABLE);
            db.execSQL(DB_QUERIES.CREATE_TABLE.IOS_TABLE);
            db.execSQL(DB_QUERIES.CREATE_TABLE.WINDOWS_TABLE);
            db.execSQL(DB_QUERIES.CREATE_TABLE.SCHEDULE_TABLE);
            db.execSQL(DB_QUERIES.CREATE_TABLE.COUNTRY_TABLE);
            Log.i("Tables Created", "Tables Created");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // TODO Auto-generated method stub

        }

    }






    public void insertScheduleRecord(String id,String title,String date,String time,String calleeNumber,
                                     String note, String status, String dummy1, String dummy2) {

        SQLiteDatabase db = my_db.getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(DB_PARAMS.COLUMNS.scheduleDetails.ID, id);

        cv.put(DB_PARAMS.COLUMNS.scheduleDetails.TITLE , title);
        cv.put(DB_PARAMS.COLUMNS.scheduleDetails.DATE, date);

        cv.put(DB_PARAMS.COLUMNS.scheduleDetails.TIME, time);
        cv.put(DB_PARAMS.COLUMNS.scheduleDetails.CALLEENUMBER, calleeNumber);
        cv.put(DB_PARAMS.COLUMNS.scheduleDetails.NOTE, note);
        cv.put(DB_PARAMS.COLUMNS.scheduleDetails.STATUS, status);
        cv.put(DB_PARAMS.COLUMNS.scheduleDetails.DUMMY1, dummy1);
        cv.put(DB_PARAMS.COLUMNS.scheduleDetails.DUMMY2, dummy2);


        db.insert(DB_PARAMS.TABLE.SCHEDULE, null, cv);



        db.close();


    }


    public Cursor getScheduleRecords() {

        Cursor c = null;

        try {
            SQLiteDatabase db = my_db.getReadableDatabase();

            c = db.rawQuery("SELECT * FROM " + DB_PARAMS.TABLE.SCHEDULE, null);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return c;

    }




    public void updateScheduleRecord(String id,String title,String date,String time,String calleeNumber,
                                     String note, String status, String dummy1, String dummy2) {

        L.i("updateRecord","updateRecord");

        SQLiteDatabase db = my_db.getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(DB_PARAMS.COLUMNS.scheduleDetails.ID, id);

        cv.put(DB_PARAMS.COLUMNS.scheduleDetails.TITLE , title);
        cv.put(DB_PARAMS.COLUMNS.scheduleDetails.DATE, date);

        cv.put(DB_PARAMS.COLUMNS.scheduleDetails.TIME, time);
        cv.put(DB_PARAMS.COLUMNS.scheduleDetails.CALLEENUMBER, calleeNumber);
        cv.put(DB_PARAMS.COLUMNS.scheduleDetails.NOTE, note);
        cv.put(DB_PARAMS.COLUMNS.scheduleDetails.STATUS, status);
        cv.put(DB_PARAMS.COLUMNS.scheduleDetails.DUMMY1, dummy1);
        cv.put(DB_PARAMS.COLUMNS.scheduleDetails.DUMMY2, dummy2);



        db.update(DB_PARAMS.TABLE.SCHEDULE, cv, DB_PARAMS.COLUMNS.details.ID + "=" + id, null);



        db.close();


    }



    public void deleteScheduleByID(int id){

            db.delete(DB_PARAMS.TABLE.SCHEDULE, DB_PARAMS.COLUMNS.details.ID + "=" + id, null);



    }



    public void insertCountryRecord(String id,String name, String code, String selected) {

        SQLiteDatabase db = my_db.getWritableDatabase();


        ContentValues cv = new ContentValues();

        cv.put(DB_PARAMS.COLUMNS.country.ID, id);

        cv.put(DB_PARAMS.COLUMNS.country.NAME, name);
        cv.put(DB_PARAMS.COLUMNS.country.CODE, code);

        cv.put(DB_PARAMS.COLUMNS.country.SELECTED, selected);

        db.insert(DB_PARAMS.TABLE.COUNTRY, null, cv);



        db.close();


    }















//
//    public void deleteCricketerRecord(String name){
//
//
//        try {
//            SQLiteDatabase db = my_db.getWritableDatabase();
//
//            db.delete(DB_PARAMS.TABLE.CRICKET, DB_PARAMS.COLUMNS.cricket.NAME + "='"+name+"'", null);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//    }
//
//    public void updateCricketerRecord(String name){
//
//
//
//
//        try {
//
//
//            SQLiteDatabase db = my_db.getWritableDatabase();
//
//            ContentValues cv = new ContentValues();
//            cv.put(DB_PARAMS.COLUMNS.cricket.NAME, name);
//
//            db.update(DB_PARAMS.TABLE.CRICKET, cv, null, null);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


//		Convenience method for updating rows in the database.
//		Parameters:
//			table the table to update in
//			values a map from column names to new column
//			 values. null is a valid value that will be translated to
//			 NULL.
//			whereClause the optional WHERE clause to apply
//			 when updating. Passing null will update all rows.
//			whereArgs You may include ?s in the where clause,
//			 which will be replaced by the values from whereArgs.
//			 The values will be bound as Strings.
//		Returns:
//			 the number of rows affected

}





