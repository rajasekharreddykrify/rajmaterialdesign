package rajmaterialdemo.com.rajmaterialdemo.db;

public class DB_PARAMS {
	
	public static String DATABASE_NAME  = "RAJ_DB";
	public static int DATABASE_VERSION  = 2;
	
	
	public interface TABLE{
		
		String ANDROID = "ANDROID_TABLE";
		String IOS = "IOS_TABLE";
		String WINDOWS = "WINDOWS_TABLE";
		String SCHEDULE = "SCHEDULE_TABLE";
		String COUNTRY = "country_TABLE";
	}


	public static class COLUMNS{
		
		public interface details{
			String ID = "id";
			String NAME = "name";
			String EMAIL = "email";
			String PLATFORM = "platform";
			String MOBILE = "mobile";
			String IMAGEURL = "imageurl";
		}

		public interface scheduleDetails{
			String ID = "id";
			String TITLE = "title";
			String DATE = "date";
			String TIME = "time";
			String CALLEENUMBER = "calleenumber";
			String NOTE = "note";
			String STATUS = "status";
			String DUMMY1 = "dummy1";
			String DUMMY2= "dummy2";

		}

		public interface country{
			String ID = "id";
			String NAME = "name";
			String CODE = "code";
			String SELECTED = "selected";
		}

		
	}
	

}
