package rajmaterialdemo.com.rajmaterialdemo.db;

public class DB_QUERIES {

    public interface CREATE_TABLE {
        String ANDROID_TABLE = "CREATE TABLE " + DB_PARAMS.TABLE.ANDROID
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + DB_PARAMS.COLUMNS.details.ID + " TEXT,"
                + DB_PARAMS.COLUMNS.details.NAME + " TEXT,"
                + DB_PARAMS.COLUMNS.details.EMAIL + " TEXT,"
                + DB_PARAMS.COLUMNS.details.MOBILE + " TEXT,"
                + DB_PARAMS.COLUMNS.details.PLATFORM + " TEXT,"
                + DB_PARAMS.COLUMNS.details.IMAGEURL + " TEXT )";


        String IOS_TABLE = "CREATE TABLE " + DB_PARAMS.TABLE.IOS
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + DB_PARAMS.COLUMNS.details.ID + " TEXT,"
                + DB_PARAMS.COLUMNS.details.NAME + " TEXT,"
                + DB_PARAMS.COLUMNS.details.EMAIL + " TEXT,"
                + DB_PARAMS.COLUMNS.details.MOBILE + " TEXT,"
                + DB_PARAMS.COLUMNS.details.PLATFORM + " TEXT,"
                + DB_PARAMS.COLUMNS.details.IMAGEURL + " TEXT )";


        String WINDOWS_TABLE = "CREATE TABLE " + DB_PARAMS.TABLE.WINDOWS
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + DB_PARAMS.COLUMNS.details.ID + " TEXT,"
                + DB_PARAMS.COLUMNS.details.NAME + " TEXT,"
                + DB_PARAMS.COLUMNS.details.EMAIL + " TEXT,"
                + DB_PARAMS.COLUMNS.details.MOBILE + " TEXT,"
                + DB_PARAMS.COLUMNS.details.PLATFORM + " TEXT,"
                + DB_PARAMS.COLUMNS.details.IMAGEURL + " TEXT )";

        String SCHEDULE_TABLE = "CREATE TABLE " + DB_PARAMS.TABLE.SCHEDULE
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + DB_PARAMS.COLUMNS.scheduleDetails.ID + " TEXT,"
                + DB_PARAMS.COLUMNS.scheduleDetails.TITLE + " TEXT,"
                + DB_PARAMS.COLUMNS.scheduleDetails.DATE + " TEXT,"
                + DB_PARAMS.COLUMNS.scheduleDetails.TIME + " TEXT,"
                + DB_PARAMS.COLUMNS.scheduleDetails.CALLEENUMBER + " TEXT,"
                + DB_PARAMS.COLUMNS.scheduleDetails.NOTE + " TEXT,"
                + DB_PARAMS.COLUMNS.scheduleDetails.STATUS + " TEXT,"
                + DB_PARAMS.COLUMNS.scheduleDetails.DUMMY1 + " TEXT,"
                + DB_PARAMS.COLUMNS.scheduleDetails.DUMMY2 + " TEXT )";

        String COUNTRY_TABLE = "CREATE TABLE " + DB_PARAMS.TABLE.COUNTRY
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + DB_PARAMS.COLUMNS.country.ID + " TEXT,"
                + DB_PARAMS.COLUMNS.country.NAME + " TEXT,"
                + DB_PARAMS.COLUMNS.country.CODE + " TEXT,"
                + DB_PARAMS.COLUMNS.country.SELECTED + " TEXT )";


    }

}
