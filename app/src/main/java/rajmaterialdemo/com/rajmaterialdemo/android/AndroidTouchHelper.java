package rajmaterialdemo.com.rajmaterialdemo.android;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

/**
 * Created by raj on 29/10/15.
 */


/**
 * Created by adammcneilly on 9/8/15.
 */
public class AndroidTouchHelper extends ItemTouchHelper.SimpleCallback {
    private AndroidAdapter androidAdapter;

    public AndroidTouchHelper(AndroidAdapter movieAdapter){
        super(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
        this.androidAdapter = movieAdapter;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        androidAdapter.swap(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        androidAdapter.remove(viewHolder.getAdapterPosition());
    }
}
