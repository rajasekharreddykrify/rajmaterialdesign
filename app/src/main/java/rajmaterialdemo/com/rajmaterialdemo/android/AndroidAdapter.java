package rajmaterialdemo.com.rajmaterialdemo.android;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import rajmaterialdemo.com.rajmaterialdemo.R;
import rajmaterialdemo.com.rajmaterialdemo.db.DB_PARAMS;
import rajmaterialdemo.com.rajmaterialdemo.helpers.AppHelper;
import rajmaterialdemo.com.rajmaterialdemo.teams.Android;
import rajmaterialdemo.com.rajmaterialdemo.user.UserDetails;

/**
 * Created by raj on 29/10/15.
 */


public class AndroidAdapter extends RecyclerView.Adapter<AndroidAdapter.ViewHolder> {
    private Context mContext;
    private List<UserDetails> users;
    private Android frag;

    public AndroidAdapter(Context context, List<UserDetails> users,Android frag) {
        this.mContext = context;
        this.users = users;
        this.frag = frag;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.android_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindUser(users.get(position));
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public void remove(int position) {
        users.remove(position);
        notifyItemRemoved(position);
    }

    public void swap(int firstPosition, int secondPosition) {
        Collections.swap(users, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView name, email, mobile, platform;
        public final ImageView image;
        public final CardView android_item_parent_cardView;

        public ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.android_item_name_tv);
            email = (TextView) view.findViewById(R.id.android_item_email_tv);
            mobile = (TextView) view.findViewById(R.id.android_item_mobile_tv);
            platform = (TextView) view.findViewById(R.id.android_item_platform_tv);

            image = (ImageView) view.findViewById(R.id.android_item_image_iv);

            android_item_parent_cardView = (CardView) view.findViewById(R.id.android_item_parent_cardView);
        }

        public void bindUser(final UserDetails ud) {
            this.name.setText(ud.getName());
            this.email.setText(ud.getEmail());
            this.mobile.setText(ud.getMobile());
            this.platform.setText(ud.getPlatform());

            if(ud.getImageurl().equals("")){
                this.image.setImageResource(R.drawable.raj_krify);
            }else{
                this.image.setImageURI(Uri.parse(ud.getImageurl()));
            }



            this.android_item_parent_cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(mContext, AndroidDetails.class);
                    in.putExtra("name", ud.getName());
                    in.putExtra("image_url", ud.getImageurl());
                    mContext.startActivity(in);
                }
            });

            this.android_item_parent_cardView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                   // AppHelper.editOptions(mContext, DB_PARAMS.TABLE.ANDROID,ud.getEmail(),ud.getId());
                    frag.editOptions(mContext, DB_PARAMS.TABLE.ANDROID,ud);


                    return false;
                }
            });

        }
    }
}
