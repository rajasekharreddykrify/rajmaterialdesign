package rajmaterialdemo.com.rajmaterialdemo.teams;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.like.LikeButton;
import com.like.OnLikeListener;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import rajmaterialdemo.com.rajmaterialdemo.R;
import rajmaterialdemo.com.rajmaterialdemo.Utilities;
import rajmaterialdemo.com.rajmaterialdemo.android.AndroidAdapter;
import rajmaterialdemo.com.rajmaterialdemo.android.AndroidTouchHelper;
import rajmaterialdemo.com.rajmaterialdemo.db.DB_PARAMS;
import rajmaterialdemo.com.rajmaterialdemo.db.RajDB;
import rajmaterialdemo.com.rajmaterialdemo.helpers.HttpFileUpload;
import rajmaterialdemo.com.rajmaterialdemo.user.Register;
import rajmaterialdemo.com.rajmaterialdemo.user.UserDetails;
import rajmaterialdemo.com.rajmaterialdemo.utils.L;

/**
 * Created by raj on 17/10/15.
 */
public class Android extends Fragment implements OnLikeListener {

    public static String TAG = "Android";

    private final String USER_AGENT = "Mozilla/5.0";
    Button click;
    String user_details_upload;
    URL url;
    HttpURLConnection conn;
    String balance_response = "";
    TextView details;
    RajDB db;
    RecyclerView android_rv;
    // Store instance variables
    private String title;
    private int page;


    LikeButton star,heart,thumb,smile;

    // newInstance constructor for creating fragment with arguments
    public static Android newInstance(int page, String title) {
        Android fragmentFirst = new Android();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("someInt", 0);
        title = getArguments().getString("someTitle");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.android_fragment, null, false);

        db = new RajDB(getActivity());
        android_rv = (RecyclerView) v.findViewById(R.id.android_recycler_view);

        details = (TextView) v.findViewById(R.id.android_details_tv);

        click = (Button) v.findViewById(R.id.android_click);

        star = (LikeButton) v.findViewById(R.id.star_button);
        heart = (LikeButton) v.findViewById(R.id.heart_button);
        thumb = (LikeButton) v.findViewById(R.id.thumb_button);
        smile = (LikeButton) v.findViewById(R.id.smile_button);

        usingCustomIcons();

        loadData();

        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // new httpApiCall().execute("");


//                Intent in = new Intent(getActivity(),Register.class);
//                startActivity(in);
            }
        });

        return v;
    }

    /*LIKE BUTTON*/

    // For using custom icons for like button

    public void usingCustomIcons() {

        //shown when the button is in its default state or when unLiked.
//        smile.setUnlikeDrawable(new BitmapDrawable(getResources(), new IconicsDrawable(getActivity(), CommunityMaterial.Icon.cmd_emoticon).colorRes(android.R.color.darker_gray).sizeDp(25).toBitmap()));

        //shown when the button is liked!
//        smile.setLikeDrawable(new BitmapDrawable(getResources(), new IconicsDrawable(getActivity(), CommunityMaterial.Icon.cmd_emoticon).colorRes(android.R.color.holo_purple).sizeDp(25).toBitmap()));
    }


    // By "implements OnLikeListener" liked and unLiked methods are overridden

    @Override
    public void liked(LikeButton likeButton) {

        if(likeButton.getId() == R.id.heart_button){
            Toast.makeText(getActivity(), "Liked!", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void unLiked(LikeButton likeButton) {
        if(likeButton.getId() == R.id.heart_button) {
            Toast.makeText(getActivity(), "Disliked!", Toast.LENGTH_SHORT).show();

        }
    }

    /*LIKE BUTTON*/

    public void loadData() {
        String data = "";

        try {
            Cursor cr = db.getAndroidRecords();

            if (cr.moveToFirst()) {
                do {
                    //data = data +  " "+ cr.getString(1)+ " "+cr.getString(2) + " " ;
                    data = data + " " + cr.getString(1) + " " + cr.getString(2)
                            + " " + cr.getString(3) + " " + cr.getString(4) +
                            " " + cr.getString(5) + " " + cr.getString(6) + "\n";
                } while (cr.moveToNext());
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

        Log.i("data ", "data is : " + data);

        //details.setText(data);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        android_rv.setLayoutManager(linearLayoutManager);

        // Setup Adapter
        AndroidAdapter androidAdapter = new AndroidAdapter(getActivity(), getUsers(), this);
        android_rv.setAdapter(androidAdapter);

        // Setup ItemTouchHelper
        ItemTouchHelper.Callback callback = new AndroidTouchHelper(androidAdapter);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(android_rv);

    }


    private List<UserDetails> getUsers() {
        List<UserDetails> usersList = new ArrayList<UserDetails>();
        //  usersList.add(new UserDetails("Harry Potter"));

        try {
            Cursor cr = db.getAndroidRecords();

            if (cr.moveToFirst()) {
                do {

//                    L.i("0000000000", cr.getString(0));
//                    L.i("1111111111", cr.getString(1));
//                    L.i("2222222222", cr.getString(2));
//                    L.i("3333333333", cr.getString(3));
//                    L.i("4444444444", cr.getString(4));
//                    L.i("5555555555", cr.getString(5));
//                    L.i("6666666666", cr.getString(6));

                    UserDetails det = new UserDetails();
                    det.setId(cr.getString(1));
                    det.setName(cr.getString(2));
                    det.setEmail(cr.getString(3));
                    det.setMobile(cr.getString(4));
                    det.setPlatform(cr.getString(5));
                    det.setImageurl(cr.getString(6));

                    usersList.add(det);
                    //data = data +  " "+ cr.getString(1)+ " "+cr.getString(2) + " " ;
                    //   data = data +" "+ cr.getString(1)+ " "+cr.getString(2) + " "+cr.getString(3)+ " "+cr.getString(4)+"\n";
                } while (cr.moveToNext());
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

        return usersList;
    }

    // HTTP POST request
    private String sendPost() throws Exception {

        String url = "http://staging.krify.com/Seperak/mobiapp/v1/forgotpassword";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        con.setRequestProperty("Authorization", "eb03065cf549d1f3d16247c3d1d443dc");

        String urlParameters = "email=rajasekhar.u@krify.net";

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());

        return response.toString();
    }

    public void UploadFile() {
        try {
            // Set your file path here
            FileInputStream fstrm = new FileInputStream(Environment.getExternalStorageDirectory().toString() + "/DCIM/file.mp4");

            // Set your server page url (and the file title/description)
            HttpFileUpload hfu = new HttpFileUpload("http://www.myurl.com/fileup.aspx", "my file title", "my file description");

            hfu.Send_Now(fstrm);

        } catch (FileNotFoundException e) {
            // Error: File not found
        }
    }

    public void editOptions(final Context act, final String table_name, final UserDetails ud) {

        L.i(TAG, "table_name is : " + table_name);
        L.i(TAG, "email_id is : " + ud.getEmail());
        final CharSequence[] items = {act.getResources().getString(R.string.update),
                act.getResources().getString(R.string.delete),
                act.getResources().getString(R.string.call),
                act.getResources().getString(R.string.email),
                act.getResources().getString(R.string.add_to_contacts),
                act.getResources().getString(R.string.cancel)};
        final AlertDialog.Builder builder = new AlertDialog.Builder(act);
        // builder.setTitle("");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:

                        Intent in = new Intent(act, Register.class);
                        in.putExtra(Register.status, Register.update);
                        in.putExtra(Register.table_name, table_name);
                        in.putExtra(Register.email_id, ud.getEmail());
                        act.startActivity(in);

                        return;

                    case 1:

                        // UserDetails usd =  db.getAndroidRecordUsingEmail(DB_PARAMS.TABLE.ANDROID, ud.getEmail());

                        //  Cursor c  = db.getCursorAndroidRecordUsingEmail(DB_PARAMS.TABLE.ANDROID, ud.getEmail());
                        Cursor c = db.getAndroidRecordUsingId(DB_PARAMS.TABLE.ANDROID, ud.getId());

                        UserDetails usdet = new UserDetails();
                        if (c.moveToFirst()) {
                            usdet.setId(c.getString(1));
                            usdet.setName(c.getString(2));
                            usdet.setEmail(c.getString(3));
                            usdet.setMobile(c.getString(4));
                            usdet.setPlatform(c.getString(5));
                            usdet.setImageurl(c.getString(6));
                        }

                        L.LogUserDetails(usdet);
                        // L.LogUserDetails(usd);

//                        L.i("record", "record details are : " +
//                                db.getAndroidRecordUsingEmail(DB_PARAMS.TABLE.ANDROID, ud.getEmail()));


                          db.delete_byID(DB_PARAMS.TABLE.ANDROID,Integer.parseInt(ud.getId()));
                        // db.delete_byEmail(DB_PARAMS.TABLE.ANDROID, ud.getEmail());

                         loadData();

                        break;

                    case 2:

                        Utilities.callViaLocalNumber(getActivity(), ud.getMobile());

                        break;

                    case 3:

                        String[] to = new String[1];
                        String[] cc = new String[1];
                        to[0] = ud.getEmail();
                        cc[0] = "";
                        String subject = "Test Email";
                        String extra_text = "Extra text";


                        Utilities.sendEmail(getActivity(), to, cc, subject, extra_text);

                        break;
                    case 4:


//Then add below code for inserting a new contact using intent
                        //create a new intent for inserting contact
                        Intent contactIntent = new Intent(ContactsContract.Intents.SHOW_OR_CREATE_CONTACT, ContactsContract.Contacts.CONTENT_URI);
                        contactIntent.setData(Uri.parse("tel:" + ud.getMobile()));//Add the mobile number here
                        contactIntent.putExtra(ContactsContract.Intents.Insert.EMAIL, ud.getEmail()); //ADD contact name here
                        contactIntent.putExtra(ContactsContract.Intents.Insert.NAME, ud.getName()); //ADD contact name here
                        contactIntent.putExtra(ContactsContract.Data.PHOTO_URI, ud.getImageurl()); //ADD contact name here
                        //Below Start activity function will display the Add contacts native screen along with your input data
                        startActivity(contactIntent);

                        break;
                    case 5:

                        break;


                    default:

                        return;
                }

            }
        });
        builder.show();
        builder.create();

    }
































   /* public String getBalanceResponse(){





        String response= "";
        try{


          //  CustomPreferences CP = new CustomPreferences(getActivity());
            //if you are using https, make sure to import java.net.HttpsURLConnection
           // url=new URL("http://staging.krify.com/Seperak/mobiapp/v1/forgotpassword/email=" );
            String urlParameters  = "email=rajasekhar.u@krify.net";
            byte[] postData       = urlParameters.getBytes( StandardCharsets.UTF_8 );
            int    postDataLength = postData.length;
            String request        = "http://staging.krify.com/Seperak/mobiapp/v1/forgotpassword";
            URL    url            = new URL( request );
            Log.i("url","url is : "+url);




//            if (url.getProtocol().toLowerCase().equals("https")) {
//               // trustAllHosts();
//                HttpURLConnection https = (HttpsURLConnection) url.openConnection();
//               // https.setHostnameVerifier(DO_NOT_VERIFY);
//                conn = https;
//            } else {
//                conn = (HttpsURLConnection) url.openConnection();
//            }
            conn.setRequestProperty ("Authorization", "eb03065cf549d1f3d16247c3d1d443dc");

            conn = (HttpsURLConnection) url.openConnection();

            //you need to encode ONLY the values of the parameters
//			String param="param1=" + URLEncoder.encode("value1″,"UTF-8″)+
//			"&param2="+URLEncoder.encode("value2″,"UTF-8″)+
//			"&param3="+URLEncoder.encode("value3″,"UTF-8″);


            conn=(HttpURLConnection)url.openConnection();
            //set the output to true, indicating you are outputting(uploading) POST data
            conn.setDoOutput(true);
            //once you set the output to true, you don’t really need to set the request method to post, but I’m doing it anyway

            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty( "charset", "utf-8");
            conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            conn.setUseCaches(false);

            try( DataOutputStream wr = new DataOutputStream( conn.getOutputStream())) {
                wr.write( postData );
            }


            //conn.usingProxy();

            //conn.setDefaultHostnameVerifier(null);

            //Android documentation suggested that you set the length of the data you are sending to the server, BUT
            // do NOT specify this length in the header by using conn.setRequestProperty("Content-Length", length);
            //use this instead.
            //conn.setFixedLengthStreamingMode(param.getBytes().length);
           // conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            //conn.addRequestProperty("Accept", "text/html");
            //send the POST out
            PrintWriter out = new PrintWriter(conn.getOutputStream());
            //out.print(param);
            out.close();

            //build the string to store the response text from the server


            //start listening to the stream
            Scanner inStream = new Scanner(conn.getInputStream());

            //process the stream and store it in StringBuilder
            while(inStream.hasNextLine())
                response+=(inStream.nextLine());

        }



        //catch some error
        catch(MalformedURLException ex){
            //Toast.makeText(VolleyHttpsDemo.this, ex.toString(), 1 ).show();

        }
        // and some more
        catch(IOException ex){

            //Toast.makeText(VolleyHttpsDemo.this, ex.toString(), 1 ).show();
        }

        Log.i("response", "response is : "+response);
        Log.d("response", "response is : "+response);
        Log.e("response", "response is : "+response);
        Log.w("response", "response is : " + response);

        return response;
    }
*/


    public class httpApiCall extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {


            try {
//                balance_response   =   getBalanceResponse();
                balance_response = sendPost();

            } catch (Exception e) {

                e.printStackTrace();
            }

            Log.i("apiCall_response", "apiCall_response is : " + balance_response);
            Log.d("apiCall_response", "apiCall_response is : " + balance_response);
            Log.e("apiCall_response", "apiCall_response is : " + balance_response);
            Log.w("apiCall_response", "apiCall_response is : " + balance_response);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

    }
}



