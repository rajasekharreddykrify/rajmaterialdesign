package rajmaterialdemo.com.rajmaterialdemo.utils;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by raj on 11/12/15.
 */
public class Util {

    public static String getCurrentTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd:MMMM:yyyy HH:mm:ss a");
        String strDate = sdf.format(c.getTime());
        return strDate;

    }

    public static String GetFormatteddate(String date) {
        String dateInString = "";
        try {

            dateInString = date; // Start date
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Calendar c = Calendar.getInstance();
            c.setTime(sdf.parse(dateInString));
            // c.add(Calendar.DATE, days);
            sdf = new SimpleDateFormat("MMMM EEEE yyyy");
            Date resultdate = new Date(c.getTimeInMillis());


            dateInString = sdf.format(resultdate);

            System.out.println("String date:" + dateInString);

            return dateInString;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dateInString;

    }
    public static String GetMonthName(String date) {
        String dateInString = "";
        try {

            dateInString = date; // Start date
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Calendar c = Calendar.getInstance();
            c.setTime(sdf.parse(dateInString));
            // c.add(Calendar.DATE, days);
            //	sdf = new SimpleDateFormat("MMMM EEEE yyyy");
            sdf = new SimpleDateFormat("MMMM");
            Date resultdate = new Date(c.getTimeInMillis());


            dateInString = sdf.format(resultdate);
            System.out.println("String GetMonthName:" + dateInString);

            return dateInString;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dateInString;

    }

    public static String GetMonth(String date) {
        String dateInString = "";
        try {

            dateInString = date; // Start date
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Calendar c = Calendar.getInstance();
            c.setTime(sdf.parse(dateInString));
            // c.add(Calendar.DATE, days);
            //	sdf = new SimpleDateFormat("MMMM EEEE yyyy");
            sdf = new SimpleDateFormat("MM");
            Date resultdate = new Date(c.getTimeInMillis());


            dateInString = sdf.format(resultdate);
            System.out.println("String GetMonth:" + dateInString);

            return dateInString;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dateInString;

    }
    public static String GetDate(String date) {
        String dateInString = "";
        try {

            dateInString = date; // Start date
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Calendar c = Calendar.getInstance();
            c.setTime(sdf.parse(dateInString));
            // c.add(Calendar.DATE, days);
            //	sdf = new SimpleDateFormat("MMMM EEEE yyyy");
            sdf = new SimpleDateFormat("dd");
            Date resultdate = new Date(c.getTimeInMillis());


            dateInString = sdf.format(resultdate);
            System.out.println("String date:" + dateInString);

            return dateInString;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dateInString;

    }
    public static String GetDayName(String date) {
        String dateInString = "";
        try {

            dateInString = date; // Start date
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Calendar c = Calendar.getInstance();
            c.setTime(sdf.parse(dateInString));
            // c.add(Calendar.DATE, days);
            //	sdf = new SimpleDateFormat("MMMM EEEE yyyy");
            sdf = new SimpleDateFormat("EEEE");
            Date resultdate = new Date(c.getTimeInMillis());


            dateInString = sdf.format(resultdate);
            System.out.println("String date:" + dateInString);

            return dateInString;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dateInString;

    }



    //Sat 11/28/2015 06:00

    public static long getCalenderDateInMilli(String date)
    {
//		String givenDateString = "Tue Apr 23 16:08:28 GMT+05:30 2013";
//
//		String givenDateString = "Sat 11/28/2015 06:00";

        L.i("calenderdate", "calenderdate :" + date);

        long calenderdateInMilliseconds=0;

        String givenDateString = date;
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MM/dd/yyyy HH:mm");
        try {
            Date mDate = sdf.parse(givenDateString);
            calenderdateInMilliseconds = mDate.getTime();
            L.i("calenderdate in milli :: ", " calenderdate in milli :" + calenderdateInMilliseconds);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return calenderdateInMilliseconds;
    }



    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }


}
