package rajmaterialdemo.com.rajmaterialdemo.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import java.util.HashMap;

public class SessionManager {
	// Shared Preferences
	SharedPreferences pref;

	// Editor for Shared preferences
	Editor editor;

	// Context
	Context _context;

	// Shared pref mode
	int PRIVATE_MODE = 0;

	// Sharedpref file name
	private static final String PREF_NAME = "KLOPrefs";

	// All Shared Preferences Keys
	private static final String IS_CHECKED= "IsChecked";
	private static final String IS_IN_DRIVE_MODE= "YES";


// User name (make variable public to access from outside)
	// public static final String KEY_NAME = "name";

	// Email address (make variable public to access from outside)

	public static final String KEY_USERNAME = "username";
	public static final String KEY_PASSWORD = "password";

	// private static final String IS_LOGIN = "IsLoggedIn";
	// private static final String KEY_GPS = "Enabled";

	// Constructor
	public SessionManager(Context context) {
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}

	/**
	 * Create login session
	 * 
	 * @return
	 * */

	public void setStoreData(String key, String value) {

		editor.putString(key, value);

		editor.commit();
	}

	public String getStoreData(String key) {

		return pref.getString(key, "");
	}

	public void removeData(String key) {
		editor.remove(key);

		editor.commit();
	}


	public void rememberUserDetails(String username, String password) {
		// Storing login value as TRUE
		editor.putBoolean(IS_CHECKED, true);

		// Storing name in pref
		// editor.putString(KEY_NAME, name);

		// Storing email in pref
		editor.putString(KEY_USERNAME, username);
		editor.putString(KEY_PASSWORD, password);

		// commit changes
		editor.commit();
	}
 

	/**
	 * Get stored session data
	 * */
	public HashMap<String, String> getUserDetails() {
		HashMap<String, String> user = new HashMap<String, String>();
		// user name
		 
		user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, ""));
		user.put(KEY_PASSWORD, pref.getString(KEY_PASSWORD, ""));
		 

		return user;
	}

	/**
	 * Clear session details
	 * */
	public void dontRememberUserDetails() {
		// Clearing all data from Shared Preferences
		editor.putBoolean(IS_CHECKED, false);
		editor.clear();
		editor.commit();

	}

	/**
	 * Quick check for login
	 * **/
	// Get Login State
	public boolean isRememberChecked() {
		return pref.getBoolean(IS_CHECKED, false);
	}
	
	
	public String getUsername(){
		return pref.getString(KEY_USERNAME, "");
	}

	public String getPassword(){
		return pref.getString(KEY_PASSWORD, "");
	}

	public void setDriveModeStatus(boolean status) {
		Log.e("@@@@@@@@@@@@", "@@@@@@@@@@@@@@@@@@ " + status);

		editor.putBoolean(IS_IN_DRIVE_MODE, status);

		editor.commit();

	}

	public boolean getDriveModeStatus(){
		return pref.getBoolean(IS_IN_DRIVE_MODE, false);
	}
}