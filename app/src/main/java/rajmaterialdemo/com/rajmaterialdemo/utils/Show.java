package rajmaterialdemo.com.rajmaterialdemo.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import rajmaterialdemo.com.rajmaterialdemo.R;
import rajmaterialdemo.com.rajmaterialdemo.cutomviews.CircularImageView;


public class Show {

	 public static void displayToast(Activity activity,String message){
		Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
	}


	public static void alertWithOk(final Activity activity, String message,
			String alert_button) {
		final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				activity);

		alertDialogBuilder
				.setMessage(message)
				.setCancelable(false)
				.setPositiveButton(activity.getResources().getString(R.string.ok),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}


	public static void alertWithOk(final Activity activity, String message) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				activity);

		alertDialogBuilder
				.setMessage(message)
				.setCancelable(false)
				.setPositiveButton(activity.getResources().getString(R.string.ok),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	public static void alertForNoInternet(final Activity activity ) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				activity);

		alertDialogBuilder
				.setMessage(activity.getResources().getString(
						R.string.no_internet_connection))
				.setCancelable(false)
				.setPositiveButton(activity.getResources().getString(R.string.ok),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}
	public static void alertApiErrorResponse(final Activity activity ) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				activity);

		alertDialogBuilder
				.setMessage(activity.getResources().getString(R.string.api_error_response))
				.setCancelable(false)
				.setPositiveButton(activity.getResources().getString(R.string.ok),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}
	public static void alertTryAgain(final Activity activity ) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				activity);

		alertDialogBuilder
				.setMessage(activity.getResources().getString(R.string.api_error_response))
				.setCancelable(false)
				.setPositiveButton(activity.getResources().getString(R.string.ok),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	public static void loadPhotoDialog(Activity act,CircularImageView imageView, int width, int height) {

		CircularImageView tempImageView = imageView;


		AlertDialog.Builder imageDialog = new AlertDialog.Builder(act);
		LayoutInflater inflater = (LayoutInflater) act.getSystemService(act.LAYOUT_INFLATER_SERVICE);

		View layout = inflater.inflate(R.layout.custom_fullimage_dialog,
				(ViewGroup) act.findViewById(R.id.layout_root));
		ImageView image = (ImageView) layout.findViewById(R.id.fullimage);
		image.setImageDrawable(tempImageView.getDrawable());
		imageDialog.setView(layout);
		imageDialog.setPositiveButton(act.getResources().getString(R.string.ok), new DialogInterface.OnClickListener(){

			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}

		});


		imageDialog.create();
		imageDialog.show();
	}
}
