package rajmaterialdemo.com.rajmaterialdemo.utils;

import android.Manifest;
import android.app.Activity;
import android.support.v4.content.ContextCompat;

/**
 * Created by raj on 29/10/15.
 */
public class Perm {


    public static int getCalenderPermission(Activity activity){
        // Assume thisActivity is the current activity
        int permissionCheck = ContextCompat.checkSelfPermission(activity,
                Manifest.permission.WRITE_CALENDAR);
        L.i("permissionCheck","permissionCheck is : "+permissionCheck);
        return permissionCheck;
    }
}
