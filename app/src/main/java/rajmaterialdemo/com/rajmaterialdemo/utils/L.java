package rajmaterialdemo.com.rajmaterialdemo.utils;

import android.util.Log;

import rajmaterialdemo.com.rajmaterialdemo.user.UserDetails;

/**
 * Created by raj on 4/9/15.
 */
public class L {


    public static void i(String tag, String result) {
        Log.i(tag, "" + result);
    }

    public static void e(String tag, String result) {
        Log.i(tag, "" + result);
    }

    public static void d(String tag, String result) {
        Log.i(tag, "" + result);
    }

    public static void w(String tag, String result) {
        Log.i(tag, "" + result);
    }

    public static void v(String tag, String result) {
        Log.i(tag, "" + result);
    }

    public static void LogUserDetails(UserDetails ud){
        L.i("Id","Id is : "+ud.getId());
        L.i("Name","Name is : "+ud.getName());
        L.i("Email","Email is : "+ud.getEmail());
        L.i("Mobile","Mobile is : "+ud.getMobile());
        L.i("Platform","Platform is : "+ud.getPlatform());
        L.i("Image Url","Image Url is : "+ud.getImageurl());

    }



}
