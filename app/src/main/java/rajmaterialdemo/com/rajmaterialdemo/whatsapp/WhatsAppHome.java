package rajmaterialdemo.com.rajmaterialdemo.whatsapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import rajmaterialdemo.com.rajmaterialdemo.R;

/**
 * Created by raj on 10/12/15.
 */
public class WhatsAppHome extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.whatsapp_home);

    }
}
