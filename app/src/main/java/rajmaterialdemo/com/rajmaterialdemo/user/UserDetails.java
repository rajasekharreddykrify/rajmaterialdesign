package rajmaterialdemo.com.rajmaterialdemo.user;

/**
 * Created by raj on 29/10/15.
 */
public class UserDetails {

    String id,name,email,mobile,platform,imageurl;

    public UserDetails() {
    }

    public UserDetails(String id,String name, String email, String mobile, String platform, String imageurl) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.mobile = mobile;
        this.platform = platform;
        this.imageurl = imageurl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }
}
