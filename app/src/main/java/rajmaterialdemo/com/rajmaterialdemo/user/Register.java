package rajmaterialdemo.com.rajmaterialdemo.user;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

import rajmaterialdemo.com.rajmaterialdemo.Home;
import rajmaterialdemo.com.rajmaterialdemo.R;
import rajmaterialdemo.com.rajmaterialdemo.db.DB_PARAMS;
import rajmaterialdemo.com.rajmaterialdemo.db.RajDB;
import rajmaterialdemo.com.rajmaterialdemo.utils.L;
import rajmaterialdemo.com.rajmaterialdemo.utils.Show;

/**
 * Created by raj on 29/10/15.
 */
public class Register extends Activity {

    public static  String TAG = "Register";

    EditText name,email,mobile;
    TextView platform;
    ImageView profile_pic;

    Button register;

    RajDB db;


    /* Intent Strings */

    public static String status = "status";
    public static String update = "update";
    public static String email_id = "email_id";
    public static String table_name = "table_name";

   /* CameraGallery */

    private File file=null;
    private static File dir = null;

    private String pathin;

    private Uri output1;

    String savedImagePath="";
   /* CameraGallery */


    Cursor cursor_via_email;

    String intent_status = "register";
    String update_user_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        db = new RajDB(Register.this);

        profile_pic = (ImageView) findViewById(R.id.register_profile_pic_iv);

        name = (EditText) findViewById(R.id.register_name_et);
        email = (EditText) findViewById(R.id.register_email_et);
        mobile = (EditText) findViewById(R.id.register_mobile_et);
        platform = (TextView) findViewById(R.id.register_platform_et);

        register = (Button) findViewById(R.id.register_signup_btn);


        try {
            Intent in = getIntent();
            L.i(TAG, "status is : " +in.getStringExtra(Register.status)  );

            intent_status = in.getStringExtra(Register.status);
            if(in.getStringExtra(Register.status).equals(Register.update)){
//                L.i("getIntent","getIntent ");
//                cursor_via_email = db.getAndroidRecordUsingEmail(in.getStringExtra(Register.table_name),
//                        in.getStringExtra(Register.email_id));
                getRecordViaEmail(in.getStringExtra(Register.email_id));
            }
        } catch (Exception e) {
            L.e(TAG, "Intent Exception");
            intent_status = "register";
            e.printStackTrace();
        }

        profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photoTakeOption();
            }
        });

        platform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPlatform();
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(intent_status.equals(Register.update)){
                    updateUser();
                }else{
                    regUser();
                }



            }
        });



    }

    public void regUser(){
        String id = "";
        if(name.getText().toString().length()==0){

        }else if(email.getText().toString().length()==0){

        }else if(mobile.getText().toString().length()==0){

        }else if(platform.getText().toString().length()==0){

        }else{

            id =  String.valueOf(db.getProfilesCount(DB_PARAMS.TABLE.ANDROID)+1);

            if(db.checkEmailAlreadyExistsOrNot(email.getText().toString().trim(),
                    platform.getText().toString())){
                Show.displayToast(Register.this, "Email Already exists");
            }else  if(db.checkPhoneNumberAlreadyExistsOrNot(mobile.getText().toString().trim(),
                    platform.getText().toString())){
                Show.displayToast(Register.this,"Phone Number Already exists");
            }else  if(savedImagePath.equals("")){
                Show.displayToast(Register.this,"Please upload your picture");
            }else{


                    db.insertRecord(id,name.getText().toString(),
                            email.getText().toString(),
                            mobile.getText().toString(),
                            platform.getText().toString(),savedImagePath);

                    Intent in = new Intent(Register.this, Home.class);
                    startActivity(in);


            }



        }
    }

    public void updateUser(){

        if(name.getText().toString().length()==0){

        }else if(email.getText().toString().length()==0){

        }else if(mobile.getText().toString().length()==0){

        }else if(platform.getText().toString().length()==0){

        }else{



            /*if(db.checkEmailAlreadyExistsOrNot(email.getText().toString().trim(),
                    platform.getText().toString())){
                Show.displayToast(Register.this, "Email Already exists");
            }else  if(db.checkPhoneNumberAlreadyExistsOrNot(mobile.getText().toString().trim(),
                    platform.getText().toString())){
                Show.displayToast(Register.this,"Phone Number Already exists");
            }else*/




            if(savedImagePath.equals("")){
                Show.displayToast(Register.this,"Please upload your picture");
            }else{


                    db.updateRecord(update_user_id,name.getText().toString(),
                            email.getText().toString(),
                            mobile.getText().toString(),
                            platform.getText().toString(),savedImagePath);

                    Intent in = new Intent(Register.this, Home.class);
                    startActivity(in);


            }



        }
    }

    public void getRecordViaEmail(String email_id){
        L.i("getAndroidRecordUsingEmail", "getAndroidRecordUsingEmail ");
        L.i("email_id", "email_id is : "+email_id);

        try {
            String data = "";
            Cursor cr = db.getAndroidRecords();

            if(cr.moveToFirst()){
                do {
                    //data = data +  " "+ cr.getString(1)+ " "+cr.getString(2) + " " ;
                    data = "ID is :"+ cr.getString(1)+ " Name is : "+cr.getString(2)
                            + " Email is :  "+cr.getString(3)+ " Mobile Number is : "+cr.getString(4)+
                            " Platform is : "+cr.getString(5)+" Image_path is : "+cr.getString(6)+" ";
                    L.i("data", "data is : "+data);
                    L.i("cr.getString(3)", "cr.getString(3) is : "+cr.getString(3));
                    L.i("email", "email is : "+email_id);

                    if(cr.getString(3).equals(email_id)){
                        update_user_id = cr.getString(1);
                        name.setText(cr.getString(2));
                        email.setText(cr.getString(3));
                        mobile.setText(cr.getString(4));
                        platform.setText(cr.getString(5));
                        profile_pic.setImageURI(Uri.parse(cr.getString(6)));
                        savedImagePath = cr.getString(6);
                    }

                } while (cr.moveToNext());
            }

            register.setText(getResources().getString(R.string.update));
        } catch (Exception e) {

            e.printStackTrace();
        }


//        name.setText(cr.getString(2));
//        email.setText(cr.getString(3));
//        mobile.setText(cr.getString(4));
//        platform.setText(cr.getString(5));
//        //profile_pic.setImageURI(Uri.parse(cr.getString(6)));
//
//        L.i("cursor", "cursor is : " + cr.getString(2));
//        L.i("cursor", "cursor is : " + cr.getString(3));
//        L.i("cursor", "cursor is : " + cr.getString(4));
//        L.i("cursor", "cursor is : " + cr.getString(5));
//        L.i("cursor","cursor is : "+cr.getString(6));
    }


    public void selectPlatform() {
        final CharSequence[] items = { getResources().getString(R.string.android),
                getResources().getString(R.string.ios),
                getResources().getString(R.string.windows) };
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // builder.setTitle("");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:

                        platform.setText(getResources().getString(R.string.android));
                        return;

                    case 1:

                        platform.setText(getResources().getString(R.string.ios));
                        break;

                    case 2:
                        platform.setText(getResources().getString(R.string.windows));
                        break;

                    default:

                        return;
                }
            }
        });
        builder.show();
        builder.create();

    }


    public void photoTakeOption() {
        final CharSequence[] items = { getResources().getString(R.string.camera), getResources().getString(R.string.gallery),
                getResources().getString(R.string.cancel) };
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // builder.setTitle("");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:

                        openNewCamera();
                        return;

                    case 1:

                        getImageFromGallery();

                        break;

                    case 2:

                        break;

                    default:

                        return;
                }

            }
        });
        builder.show();
        builder.create();

    }

    public void openNewCamera() {

        L.d("Image ", "take");



        Intent cameraIntent = new Intent(
                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        pathin = Environment.getExternalStorageDirectory()
                + "/seperak_camera"
                + String.valueOf(System.currentTimeMillis()) + ".jpg";

        output1 = Uri.fromFile(new File(pathin));

        //output1 = Uri.fromFile(mFileTemp);

        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, output1);

        startActivityForResult(cameraIntent, 0);


    }

    public void getImageFromGallery() {

        L.i("Image ", "Gallery");



        final Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 1);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        try {

            if (resultCode == Activity.RESULT_OK) {
//                Toast.makeText(SignUp.this,
//                        "RESULT_OK resultCode ",
//                        Toast.LENGTH_SHORT).show();
                if (requestCode == 0) {

//                    Toast.makeText(SignUp.this,
//                            "ifffffffff resultCode is : "+resultCode,
//                            Toast.LENGTH_SHORT).show();

                    try {

                        //final Bundle extras = data.getExtras();
                        //final Bitmap b = (Bitmap) extras.get("data");

                        if (data != null) {

                            Uri selectedImageUri = data.getData();

                            final Bitmap bitmap= getImage1(getRealPathFromURI(selectedImageUri));

                            final long time = System.currentTimeMillis();
                            // final Bitmap bit = getResizedBitmap(bitmap, 900, 1600);
                            savedImagePath = saveBitmapIntoSdcard(bitmap, time + ".jpeg");
                            updatePhotoItem(savedImagePath);

                        } else {

                            final Bitmap bitmap = getImage1(pathin);

                            final long time = System.currentTimeMillis();
                            // final Bitmap bit = getResizedBitmap(bitmap, 900, 1600);
                            savedImagePath = saveBitmapIntoSdcard(bitmap, time + ".jpeg");
                            updatePhotoItem(savedImagePath);

                        }

                        // L.e("camera saved URL :  ", " " + savedImagePath);

                    } catch (final IOException e) {

                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (requestCode == 1) {

                    try {


                        Uri selectedImageUri = data.getData();

                        final Bitmap b = getImage1(getRealPathFromURI(selectedImageUri));

                        final long time = System.currentTimeMillis();

                        // final Bitmap bit = getResizedBitmap(b, 900, 1600);




                        savedImagePath = saveBitmapIntoSdcard(b, time + ".jpeg");

                        updatePhotoItem(savedImagePath);




						/*final Uri selectedImageUri = data.getData();

						final Bitmap bitmap = BitmapFactory
								.decodeStream(getContentResolver().openInputStream(
										selectedImageUri));
						final long time = System.currentTimeMillis();
						// final Bitmap bit = getResizedBitmap(bitmap, 900, 1600);
						savedImagePath = saveBitmapIntoSdcard(bitmap, time + ".jpeg");
						updatePhotoItem(savedImagePath);*/

                        // L.e("brows saved URL :  ", " " + savedImagePath);

                    } catch (final Exception e) {

                        return;
                    }

                }






            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private String saveBitmapIntoSdcard(Bitmap bitmap22, String filename)
            throws IOException {
		/*
		 *
		 * check the path and create if needed
		 */
        createBaseDirctory();

        try {

            new Date();

            OutputStream out = null;
            file = new File(Register.dir, "/seperak" + filename);

            out = new FileOutputStream(file);

            bitmap22.compress(Bitmap.CompressFormat.JPEG, getResources().getInteger(R.integer.bitmap_compress_quality), out);

            out.flush();
            out.close();
            // Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
            return file.getAbsolutePath();
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void createBaseDirctory() {

        final String extStorageDirectory = Environment
                .getExternalStorageDirectory().toString();
        Register.dir = new File(extStorageDirectory + "/RajMaterialDemo");

        if (Register.dir.mkdir()) {
            System.out.println("Directory created");
        } else {
            System.out.println("Directory is not created or exists");
        }
    }

    private String getPath(Uri uri) {
        final String[] projection = { MediaStore.MediaColumns.DATA };
        final Cursor cursor = managedQuery(uri, projection, null, null, null);
        final int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        final int width = bm.getWidth();
        final int height = bm.getHeight();
        final float scaleWidth = (float) newWidth / width;
        final float scaleHeight = (float) newHeight / height;
        final Matrix matrix = new Matrix();

        matrix.postScale(scaleWidth, scaleHeight);
        final Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width,
                height, matrix, false);
        System.out.println("in getresizebitmap" + resizedBitmap.toString());
        return resizedBitmap;

    }


    // image Rotation

    public Bitmap getImage1(String path) throws IOException {
        Bitmap pqr = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, options);
            int srcWidth = options.outWidth;
            int srcHeight = options.outHeight;
            final int REQUIRED_SIZE = 380;

            int inSampleSize = 1;
            while (srcWidth / 2 >= REQUIRED_SIZE) {
                srcWidth /= 2;
                srcHeight /= 2;
                inSampleSize *= 2;
            }

            options.inJustDecodeBounds = false;
            options.inDither = false;
            options.inSampleSize = inSampleSize;
            options.inScaled = false;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap sampledSrcBitmap = BitmapFactory.decodeFile(path, options);
            ExifInterface exif = new ExifInterface(path);
            String s = exif.getAttribute(ExifInterface.TAG_ORIENTATION);

            Matrix matrix = new Matrix();
            float rotation = rotationForImage(Register.this,
                    Uri.fromFile(new File(path)));
            if (rotation != 0f) {
                matrix.preRotate(rotation);
            }

            pqr = Bitmap.createBitmap(sampledSrcBitmap, 0, 0, sampledSrcBitmap.getWidth(), sampledSrcBitmap.getHeight(), matrix, true);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }

        return pqr;
    }

    public float rotationForImage(Context context, Uri uri) {
        try {
            if (uri.getScheme().equals("content")) {
                String[] projection = { MediaStore.Images.ImageColumns.ORIENTATION };
                Cursor c = context.getContentResolver().query(uri, projection,
                        null, null, null);
                if (c.moveToFirst()) {
                    return c.getInt(0);
                }
            } else if (uri.getScheme().equals("file")) {
                try {
                    ExifInterface exif = new ExifInterface(uri.getPath());
                    int rotation = (int) exifOrientationToDegrees(exif
                            .getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                    ExifInterface.ORIENTATION_NORMAL));
                    return rotation;
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
        return 0f;
    }

    private static float exifOrientationToDegrees(int exifOrientation) {
        try {
            if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
                return 90;
            } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
                return 180;
            } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
                return 270;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block

            e.printStackTrace();

        }
        return 0;
    }

    public String getRealPathFromURI(Uri contentUri) {
        int column_index = 0;
        Cursor cursor = null;

        try {
            String[] proj = { MediaStore.Audio.Media.DATA };
            cursor = managedQuery(contentUri, proj, null, null, null);
            // Cursor cursor = getContentResolver().query(contentUri, proj,
            // null, null, null); //Since manageQuery is deprecated
            column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return cursor.getString(column_index);
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("file", pathin);

    }
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        pathin = savedInstanceState.getString("file");
        //  L.d("enregistred value", savedUser);

    }
    public void updatePhotoItem(final String path) {

        profile_pic.setImageURI(Uri.parse(path));

        //Toast.makeText(SignUp.this,"updatePhotoItem",Toast.LENGTH_LONG).show();

    }



}
