package rajmaterialdemo.com.rajmaterialdemo.sidemenu;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Scanner;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import rajmaterialdemo.com.rajmaterialdemo.R;
import rajmaterialdemo.com.rajmaterialdemo.helpers.WebParams;
import rajmaterialdemo.com.rajmaterialdemo.user.Register;

/**
 * Created by raj on 30/12/15.
 */
public class API23 extends Activity {

    ProgressDialog pDialog;



    String api_response;

    URL url;
//    HttpsURLConnection conn;
    HttpURLConnection conn;

    int serverResponseCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.api_23);



        new apiCall().execute("");


    }






    public class apiCall extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {


//            api_response = getApiCallResponse();

//            int api_response = uploadFile("/storage/emulated/0/video/run.png");
            //emulator
            int api_response = uploadFile("content://downloads/all_downloads/1/2015-12-30-18-16-56-2054303939.jpg");

            Log.i("api_response",
                    "api_response is : " + api_response);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showPDialog();
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            Log.i("get_user_details_url", "get_user_details response is : "+api_response);
            hidePDialog();
            try {







            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }



    public String getApiCallResponse(){





        String response= "";
        try{

//			  params.put("mobile",country_dialing_code_selected+ Mobile.getText().toString());
//              params.put("email", Email.getText().toString());
//              params.put("password", password.getText().toString());
//              params.put("country", countryname);
//              params.put("country_id", country_dialing_code_selected);

            //if you are using https, make sure to import java.net.HttpsURLConnection
            try {
                url=new URL(WebParams.WebApiRoot.get_user_details);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

//			  +"mobile="+country_dialing_code_selected+ Mobile.getText().toString()
//			  +"&email="+Email.getText().toString()+
//			  "&password="+password.getText().toString()+
//			  "&country="+countryname+
//			  "&country_id="+country_dialing_code_selected

            Log.i("url","url is : "+url);


//            if (url.getProtocol().toLowerCase().equals("https")) {
//                trustAllHosts();
//                HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
//                https.setHostnameVerifier(DO_NOT_VERIFY);
//                conn = https;
//            } else {
//                conn = (HttpsURLConnection) url.openConnection();
//            }

            conn=(HttpURLConnection)url.openConnection();

            //you need to encode ONLY the values of the parameters
//			String param="param1=" + URLEncoder.encode("value1″,"UTF-8″)+
//			"&param2="+URLEncoder.encode("value2″,"UTF-8″)+
//			"&param3="+URLEncoder.encode("value3″,"UTF-8″);


            String charset = "UTF-8";

            String s = "user_id="+URLEncoder.encode("1",charset);
                 //   "&token="+URLEncoder.encode("643e5dd8387956303d6a98742883f890",charset);
//            String s = "mobile=" + URLEncoder.encode(country_dialing_code_selected+ Mobile.getText().toString(), charset);
//            s += "&email=" + URLEncoder.encode( Email.getText().toString(), charset);
//            s += "&password=" + URLEncoder.encode( password.getText().toString(), charset);
//
//            s += "&country=" + URLEncoder.encode(countryname, charset);
//            s += "&country_id=" + URLEncoder.encode(country_dialing_code_selected, charset);
//            s += "&api_key=" + URLEncoder.encode("6fc8ubv8r5nf9", charset);
            Log.i("charset","s is : "+s);
            Log.e("charset", "s is : " + s);
            Log.d("charset", "s is : " + s);
            conn.setRequestProperty("Authorization", "395739457e9b467d499ae08706a9d7k5r");
            conn.setFixedLengthStreamingMode(s.getBytes().length);

//            PrintWriter out = new PrintWriter(conn.getOutputStream());
//            out.print(s);
//            out.close();



            conn.setConnectTimeout(WebParams.WebApiRoot.connection_timeout);
            //set the output to true, indicating you are outputting(uploading) POST data
            conn.setDoOutput(true);
            //once you set the output to true, you don’t really need to set the request method to post, but I’m doing it anyway
            conn.setRequestMethod("POST");


            //conn.usingProxy();

            //conn.setDefaultHostnameVerifier(null);

            //Android documentation suggested that you set the length of the data you are sending to the server, BUT
            // do NOT specify this length in the header by using conn.setRequestProperty("Content-Length", length);
            //use this instead.
            //conn.setFixedLengthStreamingMode(param.getBytes().length);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            //conn.addRequestProperty("Accept", "text/html");
            //send the POST out
            PrintWriter out = new PrintWriter(conn.getOutputStream());
            out.print(s);
            out.close();

            //build the string to store the response text from the server


            //start listening to the stream
            Scanner inStream = new Scanner(conn.getInputStream());

            //process the stream and store it in StringBuilder
            while(inStream.hasNextLine())
                response+=(inStream.nextLine());

        }



        //catch some error
        catch(MalformedURLException ex){
            //Toast.makeText(VolleyHttpsDemo.this, ex.toString(), 1 ).show();

        }
        // and some more
        catch(IOException ex){

            //Toast.makeText(VolleyHttpsDemo.this, ex.toString(), 1 ).show();
        }catch (Exception e){
            e.printStackTrace();
        }

        Log.i("response", "response is : "+response);
        Log.d("response", "response is : "+response);
        Log.e("response", "response is : "+response);
        Log.w("response", "response is : "+response);

        return response;
    }









    // always verify the host - dont check for certificate
    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    /**
     * Trust every server - dont check for any certificate
     */
    private static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[] {};
            }

            public void checkClientTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }
        } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection
                    .setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

















    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    private void showPDialog() {
        pDialog = new ProgressDialog(API23.this);
        // Showing progress dialog before making http request
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();
    }





    public int uploadFile(String imagepath) {

        String response = "";

        String fileName = imagepath;
        Log.e("File Name", fileName);

        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(fileName);
//        int pk = almabayDatabase.getUserID();

        if (!sourceFile.isFile()) {

//            dialog.dismiss();

            Log.e("uploadFile", "Source File not exist :" + imagepath);

            runOnUiThread(new Runnable() {
                public void run() {
                    // messageText.setText("Source File not exist :"+ imagepath);
                    Log.e("Sourcefile", "File doesn't exist");
                }
            });
            return 0;
        } else {
            try {

                // open a URL connection to the Servlet
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(WebParams.WebApiRoot.upload_image);

                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("Authorization", "395739457e9b467d499ae08706a9d7k5r");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("picture", fileName);
//                conn.setRequestProperty("user_name", "raja");
                // conn.setRequestProperty("timeline_id", String.valueOf(pk));

                dos = new DataOutputStream(conn.getOutputStream());

                // add parameter timeline_id
//                String timeline_id = String.valueOf(pk);
                Log.e("Sending", "Sending user_name");
                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"user_name\"" + lineEnd);
                dos.writeBytes(lineEnd);

                // assign value
                dos.writeBytes("Rajasekhar");
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + lineEnd);

                //Send Image
                Log.e("Sending", "Sending Image");
                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"picture\";filename=\"" + fileName + "\"" + lineEnd);
                dos.writeBytes(lineEnd);
                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {

                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();
                Log.e("ResponseCode", String.valueOf(serverResponseCode));
                String serverResponseMessage = conn.getResponseMessage();

                Log.i("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);

                if (serverResponseCode == 200) {

                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(API23.this, "File Upload Complete.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }



                //build the string to store the response text from the server


                //start listening to the stream
                Scanner inStream = new Scanner(conn.getInputStream());

                //process the stream and store it in StringBuilder
                while(inStream.hasNextLine())
                    response+=(inStream.nextLine());

                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();

            } catch (MalformedURLException ex) {

//                dialog.dismiss();
                ex.printStackTrace();

                runOnUiThread(new Runnable() {
                    public void run() {
                        //messageText.setText("MalformedURLException Exception : check script url.");
                        Toast.makeText(API23.this, "MalformedURLException", Toast.LENGTH_SHORT).show();
                    }
                });

                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (Exception e) {

//                dialog.dismiss();
                e.printStackTrace();

                runOnUiThread(new Runnable() {
                    public void run() {
                        // messageText.setText("Got Exception : see logcat ");
                        Toast.makeText(API23.this, "Got Exception : see logcat ", Toast.LENGTH_SHORT).show();
                    }
                });
                Log.e("Upload file failed", "Exception : " + e.getMessage(), e);
            }
//            dialog.dismiss();


            Log.i("response", "response is : "+response);
            Log.d("response", "response is : "+response);
            Log.e("response", "response is : "+response);
            Log.w("response", "response is : "+response);


            return serverResponseCode;

        } // End else block
    }






}
