package rajmaterialdemo.com.rajmaterialdemo.sidemenu;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import rajmaterialdemo.com.rajmaterialdemo.R;

public class DeviceContacts extends AppCompatActivity {

    ListView deviceContactsListView;
    ProgressDialog pd;

    List<DeviceContactModel> contactsList;
    DeviceContactsAdapter contactAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_contacts);

        deviceContactsListView = (ListView) findViewById(R.id.device_contacts_lv);



        new loadMobileContacts().execute("");

    }
//
//    public void submit(View view){
//
//
//        String upload_numbers_to_server = "";
//        for (int i = 0; i <contactAdapter.getNumbersChecked().size() ; i++) {
//
//            if(upload_numbers_to_server.length()==0){
//                upload_numbers_to_server = upload_numbers_to_server + ""+contactAdapter.getNumbersChecked().get(i).getNumber();
//
//            }else{
//                upload_numbers_to_server = upload_numbers_to_server + ","+contactAdapter.getNumbersChecked().get(i).getNumber();
//
//            }
//
//
//        }
//
//        Show.displayToast(DeviceContacts.this, " SelectedNumber are : "+upload_numbers_to_server);
//
//        L.i("Selected Items"," SelectedNumber "+upload_numbers_to_server);
//
//
//
//    }

    public void loadData() {
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(UserContacts.this);
//        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        contacts_rv.setLayoutManager(linearLayoutManager);

        // Setup Adapter
         contactAdapter = new DeviceContactsAdapter(DeviceContacts.this, contactsList);
        deviceContactsListView.setAdapter(contactAdapter);

        //contactAdapter.notifyDataSetChanged();

    }

    private List<DeviceContactModel> getContactsList() {
        List<DeviceContactModel> contactsList = new ArrayList<DeviceContactModel>();
        StringBuffer sb = new StringBuffer();
        sb.append("......Contact Details.....");
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        String phone = null;
        String emailContact = null;
        String emailType = null;
        String image_uri = "";
        Bitmap bitmap = null;
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {

//                UserContactDetails det = new UserContactDetails();

                DeviceContactModel model = new DeviceContactModel();


                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                image_uri = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));

                // L.i("image_uri" ,"image_uri is : "+image_uri );
                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    //System.out.println("name : " + name + ", ID : " + id);
                    sb.append("\n Contact Name:" + name);
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        sb.append("\n Phone number:" + phone);
                        // System.out.println("phone" + phone);
                    }
                    pCur.close();
                    Cursor emailCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (emailCur.moveToNext()) {
                        emailContact = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                        emailType = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
                        sb.append("\nEmail:" + emailContact + "Email type:" + emailType);
                        // System.out.println("Email " + emailContact + " Email Type : " + emailType);
                    }
                    emailCur.close();
                }
//                if (image_uri != null) {
//                    System.out.println(Uri.parse(image_uri));
//                    try {
//                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(image_uri));
//                        sb.append("\n Image in Bitmap:" + bitmap);
//                        System.out.println(bitmap);
//                    } catch (FileNotFoundException e) {
//                        // TODO Auto-generated catch block
//                        e.printStackTrace();
//                    } catch (IOException e) { // TODO Auto-generated catch block
//                        e.printStackTrace();
//
//                    }
//                }

//                det.setName(name);
//                det.setEmail(emailContact);
//                det.setEmail_type(emailType);
//                det.setPhone(phone);
//                det.setImage_url(image_uri);
//
//                if (image_uri != null) {
//                    det.setImage_url(image_uri);
//                }else{
//                    det.setImage_url("");
//                }

//                contactsList.add(det);

                model.setName(name);
                model.setNumber(phone);
                model.setStatusChecked(false);
                contactsList.add(model);


                sb.append("\n........................................");
            }
            //details.setText(sb);
        }

        return contactsList;
    }

    public class loadMobileContacts extends AsyncTask<String, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = ProgressDialog.show(DeviceContacts.this, "", "loading Contacts...");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
            loadData();
        }

        @Override
        protected Void doInBackground(String... params) {

            contactsList = getContactsList();
//            contactsList = displayContacts();
            return null;
        }
    }



    public synchronized void prepareContactsInBackground(){

        Thread contactsHandler = new Thread(new Runnable() {
            @Override
            public void run() {

            }
        });

        contactsHandler.start();

    }


}
