package rajmaterialdemo.com.rajmaterialdemo.sidemenu;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import rajmaterialdemo.com.rajmaterialdemo.R;
import rajmaterialdemo.com.rajmaterialdemo.Utilities;
import rajmaterialdemo.com.rajmaterialdemo.cutomviews.CircularImageView;
import rajmaterialdemo.com.rajmaterialdemo.utils.Show;

public class ScheduleListAdapter extends RecyclerView.Adapter<ScheduleListAdapter.ViewHolder> {
    private Context mContext;
    private Activity activity;
    private List<ScheduleModel> users;


    public ScheduleListAdapter(Context context, List<ScheduleModel> users, Activity activity) {
        this.mContext = context;
        this.users = users;
        this.activity = activity;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_schedule_list_adapter, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindUser(users.get(position));
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public void remove(int position) {
        users.remove(position);
        notifyItemRemoved(position);
    }

    public void swap(int firstPosition, int secondPosition) {
        Collections.swap(users, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView title, date, note, time;
        public final ImageView edit;

        public ViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.item_schedule_title_tv);
            date = (TextView) view.findViewById(R.id.item_schedule_date_tv);
            note = (TextView) view.findViewById(R.id.item_schedule_note_tv);
            time = (TextView) view.findViewById(R.id.item_schedule_time_tv);

            edit = (ImageView) view.findViewById(R.id.item_schedule_edit_iv);

        }

        public void bindUser(final ScheduleModel ud) {
            this.title.setText(ud.getTitle());
            this.date.setText(ud.getDate());
            this.note.setText(ud.getNote());
            this.time.setText(ud.getTime());




            this.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Show.displayToast(activity,"edit/delete");
                }
            });



        }





    }

    public   void loadPhotoDialog(final UserContactDetails ud, CircularImageView imageView, int width, int height) {

        CircularImageView tempImageView = imageView;




        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog. getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setContentView(R.layout.custom_fullimage_dialog);
        // Add your views and their functionality

        final TextView name = (TextView) dialog
                .findViewById(R.id.custom_fullimage_name);
        final TextView call = (TextView) dialog
                .findViewById(R.id.custom_call);
        final TextView message = (TextView) dialog
                .findViewById(R.id.custom_message);
        ImageView image = (ImageView) dialog.findViewById(R.id.fullimage);

        image.setImageDrawable(tempImageView.getDrawable());
        name.setText(ud.getName());

        call.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.cancel();

                Utilities.callViaLocalNumber(activity,ud.getPhone());
            }
        });

        message.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.cancel();

                // Utilities.sendSMSDirectly(activity, ud.getPhone(), "Test SMS");
                Utilities.sendSMSViaIntent(activity, ud.getPhone(), "Test SMS");
            }
        });
        dialog.show();







//        AlertDialog.Builder imageDialog = new AlertDialog.Builder(activity);
//        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
//
//        View layout = inflater.inflate(R.layout.custom_fullimage_dialog,
//                (ViewGroup) activity.findViewById(R.id.layout_root));
//        ImageView image = (ImageView) layout.findViewById(R.id.fullimage);
//
//        image.setImageDrawable(tempImageView.getDrawable());
//        imageDialog.setView(layout);
//        imageDialog.setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener(){
//
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//
//        });
//
//
//        imageDialog.create();
//        imageDialog.show();
    }
}
