package rajmaterialdemo.com.rajmaterialdemo.sidemenu;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import rajmaterialdemo.com.rajmaterialdemo.R;
import rajmaterialdemo.com.rajmaterialdemo.handlers.ViewPagerWithImagesAdapter;
import rajmaterialdemo.com.rajmaterialdemo.widgets.CirclePageIndicator;

public class ViewPagerWithImagesTutorials extends AppCompatActivity {

    int[] images = {

            R.drawable.raj,
            R.drawable.raj_krify,
            R.drawable.raja
    };

    ViewPager viewPager;

    ViewPagerWithImagesAdapter viewPagerWithImagesAdapter;

    CirclePageIndicator mIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager_with_images_tutorials);

        images =new int[] {

                R.drawable.raj,
                R.drawable.raj_krify,
                R.drawable.raja
        };
        viewPager = (ViewPager) findViewById(R.id.pager);

        viewPagerWithImagesAdapter = new ViewPagerWithImagesAdapter(ViewPagerWithImagesTutorials.this,images);

        viewPager.setAdapter(viewPagerWithImagesAdapter);

        mIndicator = (CirclePageIndicator) findViewById(R.id.pager_indicator);
        mIndicator.setViewPager(viewPager);

    }
}
