package rajmaterialdemo.com.rajmaterialdemo.sidemenu;

/**
 * Created by raj on 22/1/16.
 */
public class ScheduleModel {

    String id,title, date, note, time,calleeNumber,status;

    public ScheduleModel() {
    }

    public ScheduleModel(String id, String title, String date, String note, String time, String calleeNumber, String status) {
        this.id = id;
        this.title = title;
        this.date = date;
        this.note = note;
        this.time = time;
        this.calleeNumber = calleeNumber;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCalleeNumber() {
        return calleeNumber;
    }

    public void setCalleeNumber(String calleeNumber) {
        this.calleeNumber = calleeNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
