package rajmaterialdemo.com.rajmaterialdemo.sidemenu;

/**
 * Created by raj on 12/1/16.
 */
public class DeviceContactModel {

    String name,number;
    boolean statusChecked;

    public DeviceContactModel() {
    }

    public DeviceContactModel(String name, String number, boolean statusChecked) {
        this.name = name;
        this.number = number;
        this.statusChecked = statusChecked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public boolean isStatusChecked() {
        return statusChecked;
    }

    public void setStatusChecked(boolean statusChecked) {
        this.statusChecked = statusChecked;
    }
}
