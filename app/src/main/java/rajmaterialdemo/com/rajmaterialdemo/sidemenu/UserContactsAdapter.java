package rajmaterialdemo.com.rajmaterialdemo.sidemenu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import rajmaterialdemo.com.rajmaterialdemo.R;
import rajmaterialdemo.com.rajmaterialdemo.Utilities;
import rajmaterialdemo.com.rajmaterialdemo.android.AndroidDetails;
import rajmaterialdemo.com.rajmaterialdemo.cutomviews.CircularImageView;
import rajmaterialdemo.com.rajmaterialdemo.db.DB_PARAMS;
import rajmaterialdemo.com.rajmaterialdemo.utils.Show;


/**
 * Created by raj on 13/11/15.
 */


public class UserContactsAdapter extends RecyclerView.Adapter<UserContactsAdapter.ViewHolder> {
    private Context mContext;
    private Activity activity;
    private List<UserContactDetails> users;


    public UserContactsAdapter(Context context, List<UserContactDetails> users, Activity activity) {
        this.mContext = context;
        this.users = users;
        this.activity = activity;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.contact_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindUser(users.get(position));
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public void remove(int position) {
        users.remove(position);
        notifyItemRemoved(position);
    }

    public void swap(int firstPosition, int secondPosition) {
        Collections.swap(users, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView name, phone, email;
        public final CircularImageView image;
        public final CardView contact_item_parent_cardView;

        public ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.contact_item_name_tv);
            email = (TextView) view.findViewById(R.id.contact_item_email_tv);
            phone = (TextView) view.findViewById(R.id.contact_item_phone_tv);

            image = (CircularImageView) view.findViewById(R.id.contact_item_image_iv);

            contact_item_parent_cardView = (CardView) view.findViewById(R.id.contact_item_parent_cardView);
        }

        public void bindUser(final UserContactDetails ud) {
            this.name.setText(ud.getName());
            this.email.setText(ud.getEmail());
            this.phone.setText(ud.getPhone());


            if (ud.getImage_url().equals("")) {
                this.image.setImageResource(R.drawable.raj_krify);
            } else {
                this.image.setImageURI(Uri.parse(ud.getImage_url()));
            }

            this.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                     loadPhotoDialog(ud, image, Utilities.getDeviceWidthOneThird(activity), Utilities.getDeviceWidthOneThird(activity));

                }
            });



        }





    }

    public   void loadPhotoDialog(final UserContactDetails ud, CircularImageView imageView, int width, int height) {

        CircularImageView tempImageView = imageView;




        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog. getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setContentView(R.layout.custom_fullimage_dialog);
                  // Add your views and their functionality

                final TextView name = (TextView) dialog
				.findViewById(R.id.custom_fullimage_name);
		final TextView call = (TextView) dialog
				.findViewById(R.id.custom_call);
		final TextView message = (TextView) dialog
				.findViewById(R.id.custom_message);
        ImageView image = (ImageView) dialog.findViewById(R.id.fullimage);

        image.setImageDrawable(tempImageView.getDrawable());
		name.setText(ud.getName());

        call.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.cancel();

                Utilities.callViaLocalNumber(activity,ud.getPhone());
            }
        });

        message.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.cancel();

               // Utilities.sendSMSDirectly(activity, ud.getPhone(), "Test SMS");
                Utilities.sendSMSViaIntent(activity, ud.getPhone(), "Test SMS");
            }
        });
        dialog.show();







//        AlertDialog.Builder imageDialog = new AlertDialog.Builder(activity);
//        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
//
//        View layout = inflater.inflate(R.layout.custom_fullimage_dialog,
//                (ViewGroup) activity.findViewById(R.id.layout_root));
//        ImageView image = (ImageView) layout.findViewById(R.id.fullimage);
//
//        image.setImageDrawable(tempImageView.getDrawable());
//        imageDialog.setView(layout);
//        imageDialog.setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener(){
//
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//
//        });
//
//
//        imageDialog.create();
//        imageDialog.show();
    }
}
