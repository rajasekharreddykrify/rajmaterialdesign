package rajmaterialdemo.com.rajmaterialdemo.sidemenu;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import rajmaterialdemo.com.rajmaterialdemo.R;
import rajmaterialdemo.com.rajmaterialdemo.utils.L;
import rajmaterialdemo.com.rajmaterialdemo.utils.Show;

/**
 * Created by raj on 29/1/16.
 */

public class DeviceDetails extends Activity {
    Button device;
    TextView details,battery_details;

    TelephonyManager tel;
    int mcc,mnc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_details);

        this.registerReceiver(this.batteryInfoReceiver,	new IntentFilter(Intent.ACTION_BATTERY_CHANGED));


        device = (Button) findViewById(R.id.device);
        details = (TextView) findViewById(R.id.details);
        battery_details = (TextView)findViewById(R.id.battery_details);

        tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String networkOperator = tel.getNetworkOperator();
        L.i("networkOperator", "networkOperator is : " + networkOperator);

        if (networkOperator != null) {
            try {
                mcc = Integer.parseInt(networkOperator.substring(0, 3));
                mnc = Integer.parseInt(networkOperator.substring(3));
            } catch (Exception e) {
                e.printStackTrace();

                Show.displayToast(DeviceDetails.this, "No Sim Cards Available");
            }
        }







        //tel.getAllCellInfo() Requires API LEVEL _17
        //tel.getGroupIdLevel1() Requires API LEVEL _18
        //tel.getMmsUAProfUrl() Requires API LEVEL _19
        //tel.getMmsUserAgent() Requires API LEVEL _19

        device.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                details.setText(


                        "VERSION CODENAME is :"+android.os.Build.VERSION.CODENAME+"\n \n"+
                                "VERSION_CODES BASE is :"+android.os.Build.VERSION_CODES.BASE+"\n \n"+
                                "SDK_INT is :"+android.os.Build.VERSION.SDK_INT+"\n \n"+
                                "VERSION RELEASE is :"+android.os.Build.VERSION.RELEASE+"\n \n"+

                                "BOARD is :"+android.os.Build.BOARD+"\n \n"+
                                "BOOTLOADER is :"+android.os.Build.BOOTLOADER+"\n \n"+
                                "BRAND is :"+android.os.Build.BRAND+"\n \n"+
                                "CPU_ABI is :"+android.os.Build.CPU_ABI+"\n \n"+
                                "CPU_ABI2 is :"+android.os.Build.CPU_ABI2+"\n \n"+
                                "DEVICE is :"+android.os.Build.DEVICE+"\n \n"+
                                "DISPLAY is :"+android.os.Build.DISPLAY+"\n \n"+
                                "FINGERPRINT is :"+android.os.Build.FINGERPRINT+"\n \n"+
                                "HARDWARE is :"+android.os.Build.HARDWARE+"\n \n"+
                                "HOST is :"+android.os.Build.HOST+"\n \n"+
                                "ID is :"+android.os.Build.ID+"\n \n"+
                                "MANUFACTURER is :"+android.os.Build.MANUFACTURER+"\n \n"+
                                "Model is : "+android.os.Build.MODEL+"\n \n"+
                                "PRODUCT is :"+android.os.Build.PRODUCT+"\n \n"+
                                "RADIO is :"+android.os.Build.RADIO+"\n \n"+
                                "SERIAL is :"+android.os.Build.SERIAL+"\n \n"+
                                "TAGS is :"+android.os.Build.TAGS+"\n \n"+
                                "TIME is :"+android.os.Build.TIME+"\n \n"+
                                "TYPE is :"+android.os.Build.TYPE+"\n \n"+
                                "UNKNOWN is :"+android.os.Build.UNKNOWN+"\n \n"+
                                "USER is :"+android.os.Build.USER+"\n \n"+

                                "IMEI is :"+tel.getDeviceId().toString()+"\n \n"+
//                                "IMEI 1 is :"+tel.getDeviceId(1).toString()+"\n \n"+
//                                "IMEI 2 is :"+tel.getDeviceId(2).toString()+"\n \n"+
                                "CallState is :"+tel.getCallState()+"\n \n"+
                                "DataActivity is :"+tel.getDataActivity()+"\n \n"+
                                "DataState is :"+tel.getDataState()+"\n \n"+
                                "DeviceSoftwareVersion is :"+tel.getDeviceSoftwareVersion()+"\n \n"+
                                //"GroupIdLevel1 is :"+tel.getGroupIdLevel1()+"\n \n"+
                                "Line1Number is :"+tel.getLine1Number()+"\n \n"+
                                //"MmsUAProfUrl is :"+tel.getMmsUAProfUrl()+"\n \n"+
                                //"MmsUserAgent is :"+tel.getMmsUserAgent()+"\n \n"+
                                "NetworkCountryIso is :"+tel.getNetworkCountryIso()+"\n \n"+
                                "NetworkOperator is :"+tel.getNetworkOperator()+"\n \n"+
                                "NetworkOperatorName is :"+tel.getNetworkOperatorName()+"\n \n"+
                                "NetworkType is :"+tel.getNetworkType()+"\n \n"+
                                "PhoneType is :"+tel.getPhoneType()+"\n \n"+
                                "SimCountryIso is :"+tel.getSimCountryIso()+"\n \n"+
                                "SimOperator is :"+tel.getSimOperator()+"\n \n"+
                                "SimOperatorName is :"+tel.getSimOperatorName()+"\n \n"+
                                "SimSerialNumber is :"+tel.getSimSerialNumber()+"\n \n"+
                                "SimState is :"+tel.getSimState()+"\n \n"+
                                "SubscriberId is :"+tel.getSubscriberId()+"\n \n"+
                                "VoiceMailAlphaTag is :"+tel.getVoiceMailAlphaTag()+"\n \n"+
                                "VoiceMailNumber is :"+tel.getVoiceMailNumber()+"\n \n"+
                                //"AllCellInfo is :"+tel.getAllCellInfo()+"\n \n"+
                                "CellLocation is :"+tel.getCellLocation()+"\n \n"+
                                "NeighboringCellInfo is :"+tel.getNeighboringCellInfo()+"\n \n"+


                                "MCC is :"+mcc+"\n \n"+
                                "MNC is :"+mnc+"\n \n"+

                                "Available External MB : "+ getMMC()  +"\n \n"+


                                "Available Internal MB : "+ getInternalStorageDetails() +"\n \n"+
                                "isExternalStorageWritable : "+ isExternalStorageWritable()  +"\n \n"+
                                "isExternalStorageReadable : "+ isExternalStorageReadable()  +"\n \n"





                );

            }
        });
    }










    // TO GET DEVICE BATTERY DETAILS


    private BroadcastReceiver batteryInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            int  health= intent.getIntExtra(BatteryManager.EXTRA_HEALTH,0);
            int  icon_small= intent.getIntExtra(BatteryManager.EXTRA_ICON_SMALL,0);
            int  level= intent.getIntExtra(BatteryManager.EXTRA_LEVEL,0);
            int  plugged= intent.getIntExtra(BatteryManager.EXTRA_PLUGGED,0);
            boolean  present= intent.getExtras().getBoolean(BatteryManager.EXTRA_PRESENT);
            int  scale= intent.getIntExtra(BatteryManager.EXTRA_SCALE,0);
            int  status= intent.getIntExtra(BatteryManager.EXTRA_STATUS,0);
            String  technology= intent.getExtras().getString(BatteryManager.EXTRA_TECHNOLOGY);
            int  temperature= intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE,0);
            int  voltage= intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE,0);


            battery_details.setText(
                    "Health: "+health+"\n"+
                            "Icon Small:"+icon_small+"\n"+
                            "Level: "+level+"\n"+
                            "Plugged: "+plugged+"\n"+
                            "Present: "+present+"\n"+
                            "Scale: "+scale+"\n"+
                            "Status: "+status+"\n"+
                            "Technology: "+technology+"\n"+
                            "Temperature: "+temperature+"\n"+
                            "Voltage: "+voltage+"\n");

        }


    };


    public String getMMC(){
        String state = Environment.getExternalStorageState();
        long megAvailable = 0;
        if (state.equals(android.os.Environment.MEDIA_MOUNTED)) {
            StatFs stat = new StatFs(Environment.getExternalStorageDirectory()
                    .getPath());
            long bytesAvailable = (long) stat.getBlockSize()
                    * (long) stat.getAvailableBlocks();
            megAvailable = bytesAvailable / (1024 * 1024);
            L.e("", "Available MB : " + megAvailable);
        }

        return String.valueOf(megAvailable);
    }




    public String getInternalStorageDetails(){
//		String state = Environment.get
//		long megAvailable = 0;
//		if (state.equals(android.os.Environment.MEDIA_MOUNTED)) {
        StatFs stat = new StatFs(Environment.getDataDirectory()
                .getPath());
        long bytesAvailable = (long) stat.getBlockSize()
                * (long) stat.getAvailableBlocks();
        long megAvailable = bytesAvailable / (1024 * 1024);
        L.e("", "Available MB : " + megAvailable);
        //}

        return String.valueOf(megAvailable);
    }


    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

}