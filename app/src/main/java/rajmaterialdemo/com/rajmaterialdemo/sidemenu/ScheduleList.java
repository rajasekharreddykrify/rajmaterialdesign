package rajmaterialdemo.com.rajmaterialdemo.sidemenu;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;

import rajmaterialdemo.com.rajmaterialdemo.R;
import rajmaterialdemo.com.rajmaterialdemo.utils.L;
import rajmaterialdemo.com.rajmaterialdemo.utils.Util;

public class ScheduleList extends AppCompatActivity {

    RecyclerView schedule_rv;

    ArrayList<String> Appointmenttitle;

    ArrayList<ScheduleModel> scheduleList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_list);

        schedule_rv = (RecyclerView) findViewById(R.id.schedule_list_rv);

        scheduleList = new ArrayList<ScheduleModel>();


        ReadCalendar.readCalendar(ScheduleList.this);




    }

    public void getR(String dayNametest,String month,String day,
                     String year,String strFromTimecalenderformate){
        long calenderdateinmilli = Util
                .getCalenderDateInMilli(dayNametest
                        + " "
                        + month
                        + "/"
                        + day
                        + "/"
                        + year
                        + " "
                        + strFromTimecalenderformate);
        L.i("setting_calenderdateinmilli",
                "setting_calenderdateinmilli :"
                        + calenderdateinmilli);
    }


    public void loadList(){


    }




    @SuppressLint("NewApi")
    public boolean DoEventExits(long startTimeInMillis, long endTimeInMillis,
                                String calenderDescription,String appointmentTitle) {

        ContentResolver resolver = this.getContentResolver();

        String[] items1 = { CalendarContract.Calendars._ID,
                CalendarContract.Events.TITLE,
                CalendarContract.Events.DESCRIPTION,
                CalendarContract.Events.DTSTART, CalendarContract.Events.DTEND,
                CalendarContract.Events.EVENT_LOCATION };

/*		Cursor calendarCursor2 = CalendarContract.Instances.query(resolver,
				items1, startTimeInMillis, endTimeInMillis);*/


        Uri content = Uri.parse("content://com.android.calendar/events");

        String selectionClause=CalendarContract.Events.TITLE+" = ? AND "
                +CalendarContract.Events.DTSTART+" = ? AND "
                +CalendarContract.Events.DTEND+" = ?";
        String []selectionArgs=new String[]{appointmentTitle,""+startTimeInMillis,""+endTimeInMillis};


        Cursor calendarCursor2=
                resolver.query(content, items1, selectionClause, selectionArgs, null);
		/*getTimeOftheEvents(calendarCursor2, startTimeInMillis, endTimeInMillis,
				calenderDescription);*/

        getTimeOftheEvents(calendarCursor2);

        String data2 = DatabaseUtils.dumpCursorToString(calendarCursor2);

        //Log.e("ALLEVENTS OF THE DAY", data2);

        if (calendarCursor2 != null) {
            if (calendarCursor2.moveToFirst()) {

                String eventtitle=calendarCursor2.getString(calendarCursor2.getColumnIndex("title"));

                // Toast.makeText(Setting.this, "if work :"+CalendarContract.Events.DESCRIPTION, 3000).show();
                Log.i("EVENTTitle :", "EVENTTitle :" + eventtitle);

                Log.i("AppointmentTitle :", "AppointmentTitle :"+appointmentTitle);

                if((TextUtils.equals(eventtitle, appointmentTitle))){

                    return false;
                }
                else
                {
                    return true;
                }

            } else {
                // Toast.makeText(Setting.this, "else", 3000).show();
                return true;
            }
        } else {
            // Toast.makeText(Setting.this, "main else work", 3000).show();
            return true;
        }

    }

    // To get time of the event

    public void getTimeOftheEvents(Cursor cursor){
        // startDate = new ArrayList<Calendar>();
        // endDate = new ArrayList<Calendar>();

        Appointmenttitle = new ArrayList<String>();

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {

                    String Apptitle = cursor.getString(cursor
                            .getColumnIndex(CalendarContract.Events.TITLE));

                    long starttime = cursor.getLong(cursor
                            .getColumnIndex(CalendarContract.Events.DTSTART));
                    long endtime = cursor.getLong(cursor
                            .getColumnIndex(CalendarContract.Events.DTEND));

                    ScheduleModel model = new ScheduleModel();

                    model.setTitle(Apptitle);
                    model.setDate(""+starttime);
                    model.setTime(""+endtime);
                    model.setNote(""+cursor.getPosition());

                    scheduleList.add(model);

                    Appointmenttitle.add(Apptitle);


                } while (cursor.moveToNext());
            }


			/*getStartDateArraylistValues(apiStartTimeInMilliset,
					apiEndTimeInMilliset, calenderDescription);*/

        }
    }
}
