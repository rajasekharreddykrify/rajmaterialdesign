package rajmaterialdemo.com.rajmaterialdemo.sidemenu;

/**
 * Created by raj on 13/11/15.
 */
public class UserContactDetails {

    String name,phone,email,email_type,image_url;

    public UserContactDetails() {
    }

    public UserContactDetails(String name, String phone, String email, String email_type, String image_url) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.email_type = email_type;
        this.image_url = image_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail_type() {
        return email_type;
    }

    public void setEmail_type(String email_type) {
        this.email_type = email_type;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
