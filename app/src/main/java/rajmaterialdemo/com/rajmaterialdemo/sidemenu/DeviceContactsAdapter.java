package rajmaterialdemo.com.rajmaterialdemo.sidemenu;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import rajmaterialdemo.com.rajmaterialdemo.R;

/**
 * Created by raj on 12/1/16.
 */
public class DeviceContactsAdapter extends BaseAdapter {

    int count = 0;
    private Activity activity;
    private LayoutInflater inflater;
    private List<DeviceContactModel> activityItems;
    private List<DeviceContactModel> checkedItems;

    public DeviceContactsAdapter(Activity activity, List<DeviceContactModel> activityItems) {
        this.activity = activity;
        this.activityItems = activityItems;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        View view = convertView;

        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (view == null) {
            view = inflater.inflate(R.layout.device_contact_item, null);

            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) view.findViewById(R.id.device_contact_item_name_tv);
            viewHolder.number = (TextView) view.findViewById(R.id.device_contact_item_phone_number_tv);

            viewHolder.cb = (CheckBox) view.findViewById(R.id.device_contact_item_checkbox);


            view.setTag(viewHolder);
            view.setTag(R.id.device_contact_item_name_tv, viewHolder.name);
            view.setTag(R.id.device_contact_item_phone_number_tv, viewHolder.number);
            view.setTag(R.id.device_contact_item_checkbox, viewHolder.cb);


            DeviceContactModel model = activityItems.get(position);

            viewHolder.name.setText(model.getName());
            viewHolder.number.setText(model.getNumber());

//            viewHolder.cb.setOnCheckedChangeListener(null);

            viewHolder.cb.setChecked(activityItems.get(position).isStatusChecked());


        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.cb.setTag(position); // This line is important.

        viewHolder.name.setText(activityItems.get(position).getName());
        viewHolder.number.setText(activityItems.get(position).getNumber());
        viewHolder.cb.setChecked(activityItems.get(position).isStatusChecked());


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (activityItems.get(position).isStatusChecked()) {
                    activityItems.get(position).setStatusChecked(false);

                } else {

                    if (isInRange()) {
                        activityItems.get(position).setStatusChecked(true);
                    } else {
                        Toast.makeText(activity, "Can select only 5", Toast.LENGTH_SHORT).show();
                    }

                }

                notifyDataSetChanged();


            }
        });


        return view;
    }

    @Override
    public Object getItem(int position) {
        return activityItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return activityItems.size();
    }


    public boolean isInRange() {
        int no_of_checked = 0;
        checkedItems = new ArrayList<DeviceContactModel>();

        for (int i = 0; i < activityItems.size(); i++) {

            if (activityItems.get(i).isStatusChecked()) {
                no_of_checked = no_of_checked + 1;
                checkedItems.add(activityItems.get(i));
                if (no_of_checked >= 5) {
                    return false;
                }
            }

        }
        if (no_of_checked >= 5) {
            return false;
        }


        return true;
    }

    public List<DeviceContactModel> getNumbersChecked() {
        isInRange();

        return checkedItems;
    }

    static class ViewHolder {
        protected TextView name, number;
        protected CheckBox cb;
    }

}
