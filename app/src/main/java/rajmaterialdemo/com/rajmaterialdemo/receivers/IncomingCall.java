package rajmaterialdemo.com.rajmaterialdemo.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.android.internal.telephony.ITelephony;

import java.lang.reflect.Method;

/**
 * Created by raj on 22/2/16.
 */
public class IncomingCall extends BroadcastReceiver {

    Context ctx;
      ITelephony telephonyService;

   public void onReceive(Context context, Intent intent) {

        try {

            ctx = context;


//            if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.EXTRA_STATE_RINGING)) {
//                // This code will execute when the phone has an incoming call
//
//                // get the phone number
//                String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
//                Toast.makeText(context, "Call from:" +incomingNumber, Toast.LENGTH_LONG).show();
//
//            } else if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
//                    TelephonyManager.EXTRA_STATE_IDLE)
//                    || intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
//                    TelephonyManager.EXTRA_STATE_OFFHOOK)) {
//                // This code will execute when the call is disconnected
//                Toast.makeText(context, "Detected call hangup event", Toast.LENGTH_LONG).show();
//
//            }













            // TELEPHONY MANAGER class object to register one listner
            TelephonyManager tmgr = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);

            //Create Listner
            MyPhoneStateListener PhoneListener = new MyPhoneStateListener();

            // Register listener for LISTEN_CALL_STATE
            tmgr.listen(PhoneListener, PhoneStateListener.LISTEN_CALL_STATE);



//            TelephonyManager telephony = (TelephonyManager)
//                    context.getSystemService(Context.TELEPHONY_SERVICE);
            try {
                Class c = Class.forName(tmgr.getClass().getName());
                Method m = c.getDeclaredMethod("getITelephony");
                m.setAccessible(true);
                telephonyService = (com.android.internal.telephony.ITelephony) m.invoke(tmgr);
                telephonyService.silenceRinger();
                telephonyService.endCall();


            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            Log.e("Phone Receive Error", " " + e);
        }

    }

    private class MyPhoneStateListener extends PhoneStateListener {

        public void onCallStateChanged(int state, String incomingNumber) {

            Log.d("MyPhoneListener",state+"   incoming no:"+incomingNumber);

            if (state == 1) {

                String msg = "New Phone Call Event. Incomming Number : "+incomingNumber;
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(ctx, msg, duration);
                toast.show();

            }
        }
    }


}