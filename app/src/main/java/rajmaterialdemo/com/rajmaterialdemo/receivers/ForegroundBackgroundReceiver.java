package rajmaterialdemo.com.rajmaterialdemo.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class ForegroundBackgroundReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Toast.makeText(context, "ApplicationReceiver : " + intent.getAction(), Toast.LENGTH_SHORT).show();

    }
}
