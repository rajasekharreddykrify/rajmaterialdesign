package rajmaterialdemo.com.rajmaterialdemo.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

 /*to get notified when any app is installed*/


public class ApplicationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        Uri uri = intent.getData();
        String package_name = uri != null ? uri.getSchemeSpecificPart() : null;

        Toast.makeText(context, "ApplicationReceiver : "+package_name+" is "+intent.getAction(), Toast.LENGTH_SHORT).show();
    }
}
