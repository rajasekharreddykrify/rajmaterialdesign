package rajmaterialdemo.com.rajmaterialdemo.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import java.util.HashMap;

import rajmaterialdemo.com.rajmaterialdemo.db.RajDB;

/**
 * Created by raj on 22/1/16.
 */
public class AlarmReceiver extends BroadcastReceiver {
    String TAG = "Alarm Service";

    Context ctx;
    //    SessionManager local;
    HashMap<String, String> user;

    String latitude, longitude, token;

    RajDB db;

//    protected ConnectionDetector cd;

    // LocalBroadcastManager broadcaster;
    @Override
    public void onReceive(Context context, Intent intent) {

        ctx = context;

        db = new RajDB(ctx);
        // TODO Auto-generated method stub
        // sm = new SessionManager(context);
        // broadcaster = LocalBroadcastManager.getInstance(context);
        Log.e("AlaramService", "AlaramService");
        Log.e("alaram_time", "alaram_time is : "+intent.getStringExtra("alaram_time"));
        Log.e("RQS_1", "RQS_1 is : "+intent.getStringExtra("RQS_1"));


        Toast.makeText(context, "Reminder Received  at " + intent.getStringExtra("alaram_time"), Toast.LENGTH_LONG).show();

        cancelAlarm(context,Integer.parseInt(intent.getStringExtra("RQS_1")));


        // sendResult();

//

    }

    // public void sendResult() {
    // Intent ii = new Intent("com.krify.parkshark.ParkNow");
    // broadcaster.sendBroadcast(ii);
    //
    //
    // }



    private void cancelAlarm(Context context,int RQS_1){

        Log.e("cancelAlarm",
                "***\n"
                        + "Alarm Cancelled! \n"
                        + "***");

        Log.e("RQS_1", "RQS_1 is : "+RQS_1);


        Toast.makeText(context,"***\n"
                + "Alarm Cancelled! \n"
                + "***",Toast.LENGTH_SHORT).show();


        Intent intent = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, RQS_1, intent, 0);
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);

        db.deleteScheduleByID(RQS_1);

    }


}