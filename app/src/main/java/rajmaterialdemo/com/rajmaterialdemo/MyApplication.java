package rajmaterialdemo.com.rajmaterialdemo;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

public class MyApplication extends MultiDexApplication implements
        Application.ActivityLifecycleCallbacks {
    private static final String TAG = "MyApplication";

    @Override
    public void onCreate() {
        super.onCreate();

        registerActivityLifecycleCallbacks(this);
//        Stetho.initializeWithDefaults(this);
//        ActiveAndroid.initialize(this);
    }



    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        Log.e(TAG, "onActivityCreated  : "+activity.getClass().getSimpleName() );
//        Toast.makeText(MyApplication.this, "onActivityCreated : "+activity.getClass().getSimpleName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityStarted(Activity activity) {
        Log.e(TAG, "onActivityStarted  : "+activity.getClass().getSimpleName() );
//        Toast.makeText(MyApplication.this, "onActivityStarted : "+activity.getClass().getSimpleName(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onActivityResumed(Activity activity) {
        Log.e(TAG, "onActivityResumed:  : "+activity.getClass().getSimpleName());
//        Toast.makeText(MyApplication.this, "onActivityResumed : "+activity.getClass().getSimpleName(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onActivityPaused(Activity activity) {
        Log.e(TAG, "onActivityPaused:  : "+activity.getClass().getSimpleName() );
//        Toast.makeText(MyApplication.this, "onActivityPaused : "+activity.getClass().getSimpleName(), Toast.LENGTH_SHORT).show();
//        if(Util.isAppIsInBackground(activity)){
//            Toast.makeText(MyApplication.this, " App Is In Background", Toast.LENGTH_SHORT).show();
//
//        }else{
//            Toast.makeText(MyApplication.this, "App Is In Foreground", Toast.LENGTH_SHORT).show();
//
//        }
    }

    @Override
    public void onActivityStopped(Activity activity) {
        Log.e(TAG, "onActivityStopped:  : "+activity.getClass().getSimpleName() );
//        Toast.makeText(MyApplication.this, "onActivityStopped : "+activity.getClass().getSimpleName(), Toast.LENGTH_SHORT).show();
//        if(Util.isAppIsInBackground(activity)){
//            Toast.makeText(MyApplication.this, " App Is In Background", Toast.LENGTH_SHORT).show();
//
//        }else{
//            Toast.makeText(MyApplication.this, "App Is In Foreground", Toast.LENGTH_SHORT).show();
//
//        }
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        Log.e(TAG, "onActivitySaveInstanceState:  : "+activity.getClass().getSimpleName() );
//        Toast.makeText(MyApplication.this, "onActivitySaveInstanceState : "+activity.getClass().getSimpleName(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Log.e(TAG, "onActivityDestroyed:  : "+activity.getClass().getSimpleName() );
//        Toast.makeText(MyApplication.this, "onActivityDestroyed : "+activity.getClass().getSimpleName(), Toast.LENGTH_SHORT).show();
//
//        if(Util.isAppIsInBackground(activity)){
//            Toast.makeText(MyApplication.this, " App Is In Background", Toast.LENGTH_SHORT).show();
//
//        }else{
//            Toast.makeText(MyApplication.this, "App Is In Foreground", Toast.LENGTH_SHORT).show();
//
//        }
    }
}

