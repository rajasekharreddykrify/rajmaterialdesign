package rajmaterialdemo.com.rajmaterialdemo.handlers;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import rajmaterialdemo.com.rajmaterialdemo.R;

public class ViewPagerWithImagesAdapter extends PagerAdapter {

    Context context;
    int[] images;
    LayoutInflater layoutInflater;

    public ViewPagerWithImagesAdapter(Context context, int[] images) {
        this.context = context;
        this.images = images;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        View view = layoutInflater.inflate(R.layout.item_viewpager_with_images,
                container,false);

        ImageView item_view_pager_with_images_iv = (ImageView) view.findViewById(R.id.item_view_pager_with_images_iv);

        item_view_pager_with_images_iv.setImageResource(images[position]);
        ((ViewPager) container).addView(view);
        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {


        ((ViewPager) container).removeView((RelativeLayout)object);
    }
}
