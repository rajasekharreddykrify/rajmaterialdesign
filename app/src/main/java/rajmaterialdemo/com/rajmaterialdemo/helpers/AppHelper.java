package rajmaterialdemo.com.rajmaterialdemo.helpers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import rajmaterialdemo.com.rajmaterialdemo.R;
import rajmaterialdemo.com.rajmaterialdemo.db.DB_PARAMS;
import rajmaterialdemo.com.rajmaterialdemo.user.Register;
import rajmaterialdemo.com.rajmaterialdemo.utils.L;

/**
 * Created by raj on 10/11/15.
 */
public class AppHelper {

    public static  String TAG = "AppHelper";

    public static void editOptions(final Context act, final String table_name, final String email_id,final String id) {

        L.i(TAG, "table_name is : " +table_name  );
        L.i(TAG, "email_id is : " +email_id  );
        final CharSequence[] items = { act.getResources().getString(R.string.update),
                act.getResources().getString(R.string.delete),
                act.getResources().getString(R.string.cancel) };
        final AlertDialog.Builder builder = new AlertDialog.Builder(act);
        // builder.setTitle("");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:

                        Intent in = new Intent(act, Register.class);
                        in.putExtra(Register.status,Register.update);
                        in.putExtra(Register.table_name,table_name);
                        in.putExtra(Register.email_id,email_id);
                        act.startActivity(in);

                        return;

                    case 1:



                        break;

                    case 2:

                        break;

                    default:

                        return;
                }

            }
        });
        builder.show();
        builder.create();

    }
}
