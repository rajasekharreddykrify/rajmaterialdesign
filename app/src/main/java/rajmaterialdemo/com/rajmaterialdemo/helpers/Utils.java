package rajmaterialdemo.com.rajmaterialdemo.helpers;

import java.io.InputStream;
import java.io.OutputStream;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ListAdapter;
import android.widget.ListView;


public class Utils {

	protected static LocationManager locationManager;

	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
		}
	}

	public static void alertLoginRequired(final Context activity) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				activity);

		alertDialogBuilder
				.setMessage("You have to login to access this feature")
				.setCancelable(false)
				.setPositiveButton("Login",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();

								/*Intent activity_Intent = new Intent(activity,
										Welcome.class);
								activity.startActivity(activity_Intent);*/

							}
						})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {

								dialog.cancel();
							}
						});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}



	public static boolean isGpsTurnedOn(Activity activity) {

		locationManager = (LocationManager) activity
				.getSystemService(Context.LOCATION_SERVICE);
		if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			return true;
		} else {

			return false;
		}
	}

	public static void getTotalHeightofListView(ListView listView) {

		ListAdapter mAdapter = listView.getAdapter();

		int totalHeight = 0;

		for (int i = 0; i < mAdapter.getCount(); i++) {
			View mView = mAdapter.getView(i, null, listView);

			mView.measure(
					View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),

					View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

			totalHeight += mView.getMeasuredHeight();
			Log.w("HEIGHT" + i, String.valueOf(totalHeight));

		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (mAdapter.getCount() - 1));
		listView.setLayoutParams(params);
		listView.requestLayout();

	}

	public static int getDeviceHeight(Activity activity){

		ViewGroup.LayoutParams params;
		WindowManager w;
		Display d;
		DisplayMetrics metrics;

		w = activity.getWindowManager();
		d = w.getDefaultDisplay();
		metrics = new DisplayMetrics();
		d.getMetrics(metrics);

//        Log.i("height", "height is : " + d.getHeight());
//        Log.i("width","width is : "+d.getWidth());

		return d.getHeight();


	}
	public static int getDeviceWidth(Activity activity){

		ViewGroup.LayoutParams params;
		WindowManager w;
		Display d;
		DisplayMetrics metrics;

		w = activity.getWindowManager();
		d = w.getDefaultDisplay();
		metrics = new DisplayMetrics();
		d.getMetrics(metrics);

//        params = viewHolder.exp_cat_rl.getLayoutParams();
//        params.height = d.getHeight() - d.getHeight() / 4;

//        Log.i("height", "height is : " + d.getHeight());
//        Log.i("width","width is : "+d.getWidth());

		return d.getWidth();

	}

	public static int getDeviceWidthOnThird(Activity activity){

		ViewGroup.LayoutParams params;
		WindowManager w;
		Display d;
		DisplayMetrics metrics;

		w = activity.getWindowManager();
		d = w.getDefaultDisplay();
		metrics = new DisplayMetrics();
		d.getMetrics(metrics);

//        params = viewHolder.exp_cat_rl.getLayoutParams();
//        params.height = d.getHeight() - d.getHeight() / 4;

//        Log.i("height", "height is : " + d.getHeight());
//        Log.i("width","width is : "+d.getWidth());

		return d.getWidth()/3;

	}


	public static void setViewHeightHalfWidth(View v,Activity activity){

		ViewGroup.LayoutParams params;


		params =  v.getLayoutParams();
		params.height =  (getDeviceWidth(activity)/2);


	}
	public static void setViewHeightOneThirdWidth(View v,Activity activity){

		ViewGroup.LayoutParams params;


		params =  v.getLayoutParams();
		params.height =  (getDeviceWidth(activity)/3);


	}
	public static void setViewHeightOneFourthWidth(View v,Activity activity){

		ViewGroup.LayoutParams params;


		params =  v.getLayoutParams();
		params.height =  (getDeviceWidth(activity)/4);


	}
	public static void setViewHeightOneFourth(View v,Activity activity){

		ViewGroup.LayoutParams params;


		params =  v.getLayoutParams();
		params.height =  getDeviceHeight(activity)/4;


	}

	public static void setViewHeightOneThird(View v,Activity activity){

		ViewGroup.LayoutParams params;


		params =  v.getLayoutParams();
		params.height =  getDeviceHeight(activity)/3;


	}


}
