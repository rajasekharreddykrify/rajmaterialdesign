package rajmaterialdemo.com.rajmaterialdemo.reminder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import rajmaterialdemo.com.rajmaterialdemo.R;
import rajmaterialdemo.com.rajmaterialdemo.db.RajDB;
import rajmaterialdemo.com.rajmaterialdemo.receivers.AlarmReceiver;
import rajmaterialdemo.com.rajmaterialdemo.sidemenu.ScheduleListAdapter;
import rajmaterialdemo.com.rajmaterialdemo.sidemenu.ScheduleModel;
import rajmaterialdemo.com.rajmaterialdemo.utils.L;
import rajmaterialdemo.com.rajmaterialdemo.utils.SessionManager;
import rajmaterialdemo.com.rajmaterialdemo.utils.Util;

public class ScheduleReminders extends AppCompatActivity {


    RajDB db;

    String calleNumber = "8019519619";
    String status = "RUN";

    String note  = "This is my reminder schedule to call my friend automatically";


    RecyclerView schedule_reminder_rv;


    final static int RQS_1 = 1;
    TimePicker myTimePicker;
    Button buttonstartSetDialog;
    TextView textAlarmPrompt;
    TimePickerDialog timePickerDialog;
    TextView alarms_status;
    Button setupAlarmBtn,share;
    TimePickerDialog.OnTimeSetListener onTimeSetListener
            = new TimePickerDialog.OnTimeSetListener() {

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            Calendar calNow = Calendar.getInstance();
            Calendar calSet = (Calendar) calNow.clone();

            calSet.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calSet.set(Calendar.MINUTE, minute);
            calSet.set(Calendar.SECOND, 0);
            calSet.set(Calendar.MILLISECOND, 0);

            if (calSet.compareTo(calNow) <= 0) {
                //Today Set time passed, count to tomorrow
                calSet.add(Calendar.DATE, 1);
            }

            setAlarm(calSet);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_reminders);


        db = new RajDB(ScheduleReminders.this);

        schedule_reminder_rv = (RecyclerView) findViewById(R.id.schedule_reminder_rv);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });



        alarms_status = (TextView) findViewById(R.id.alarms_status_tv);

        setupAlarmBtn = (Button) findViewById(R.id.set_alarm_btn);
        share = (Button) findViewById(R.id.share_message_btn);

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                AppUtils.shareMessageViaApps(ScheduleReminders.this,"Hi this is a test message to share via applications");
            }
        });
        setupAlarmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePickerDialog(false);
            }
        });

//        final int _id = (int) System.currentTimeMillis();
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTimeInMillis(System.currentTimeMillis());
//        // Set the alarm's trigger time to 8:30 a.m.
//        calendar.set(Calendar.HOUR_OF_DAY, 12);
//        calendar.set(Calendar.MINUTE, 23);
//        scheduleAlarm(ScheduleReminders.this, _id, calendar);

//        setAlarm();
        // setMultipleAlarms(ScheduleReminders.this);


//        getSchedulesDataFromDB();

        loadData();

    }



    private List<ScheduleModel> getSchedulesDataFromDB() {
        List<ScheduleModel> scheduleList = new ArrayList<ScheduleModel>();
        //  usersList.add(new UserDetails("Harry Potter"));

        try {
            Cursor cr = db.getScheduleRecords();

            if (cr.moveToFirst()) {
                do {
//        insertScheduleRecord(String id, String title, String date, String time, String calleeNumber,
//                String note, String status, String dummy1, String dummy2)

                    L.i("0000000000", cr.getString(0));
                    L.i("1111111111 - id ", cr.getString(1));
                    L.i("2222222222 - title ", cr.getString(2));
                    L.i("3333333333 - date ", cr.getString(3));
                    L.i("4444444444 - time ", cr.getString(4));
                    L.i("5555555555 - calleeNumber ", cr.getString(5));
                    L.i("6666666666 - note ", cr.getString(6));
                    L.i("7777777777 - status ", cr.getString(7));
                    L.i("8888888888 - dummy1 ", cr.getString(8));
                    L.i("9999999999 - dummy2 ", cr.getString(9));

                    ScheduleModel det = new ScheduleModel();
                    det.setId(cr.getString(1));
                    det.setTitle(cr.getString(2));
                    det.setDate(cr.getString(3));
                    det.setTime(cr.getString(4));
                    det.setCalleeNumber(cr.getString(5));
                    det.setNote(cr.getString(6));
                    det.setStatus(cr.getString(7));
//
                    scheduleList.add(det);
                    //data = data +  " "+ cr.getString(1)+ " "+cr.getString(2) + " " ;
                    //   data = data +" "+ cr.getString(1)+ " "+cr.getString(2) + " "+cr.getString(3)+ " "+cr.getString(4)+"\n";
                } while (cr.moveToNext());
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

        return scheduleList;
    }



    public void loadData() {
//        String data = "";
//
//        try {
//            Cursor cr =   db.getScheduleRecords();
//
//            if (cr.moveToFirst()) {
//                do {
//                    //data = data +  " "+ cr.getString(1)+ " "+cr.getString(2) + " " ;
//                    data = data + " " + cr.getString(1) + " " + cr.getString(2)
//                            + " " + cr.getString(3) + " " + cr.getString(4) +
//                            " " + cr.getString(5) + " " + cr.getString(6) +
//                            cr.getString(7) + cr.getString(8) + cr.getString(9) +"\n";
//                } while (cr.moveToNext());
//            }
//        } catch (Exception e) {
//
//            e.printStackTrace();
//        }
//
//        Log.i("data ", "data is : " + data);

        //details.setText(data);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ScheduleReminders.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        schedule_reminder_rv.setLayoutManager(linearLayoutManager);

        // Setup Adapter
        ScheduleListAdapter androidAdapter = new ScheduleListAdapter(ScheduleReminders.this, getSchedulesDataFromDB(), this);
        schedule_reminder_rv.setAdapter(androidAdapter);

        // Setup ItemTouchHelper
//        ItemTouchHelper.Callback callback = new AndroidTouchHelper(androidAdapter);
//        ItemTouchHelper helper = new ItemTouchHelper(callback);
//        helper.attachToRecyclerView(android_rv);

    }



    public static void scheduleAlarm(Context context, int _id, Calendar calendar) {

        Log.e("UTILS", "scheduleAlarm");

        SessionManager sm = new SessionManager(context);

        Intent intent = new Intent(context, AlarmReceiver.class);
//        PendingIntent sender = PendingIntent
//                .getBroadcast(context, 0, intent, 0);

        PendingIntent sender = PendingIntent.getBroadcast(context, _id, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // We want the alarm to go off 5 seconds from now.
//        long firstTime = SystemClock.elapsedRealtime();
//        firstTime += 10 * 1000;
//        Log.e("firstTime", "" + firstTime);

        long repeating = 1 * 10 * 1000;


//        Calendar calendar = Calendar.getInstance();
//        calendar.setTimeInMillis(System.currentTimeMillis());
//        // Set the alarm's trigger time to 8:30 a.m.
//        calendar.set(Calendar.HOUR_OF_DAY, 12);
//        calendar.set(Calendar.MINUTE, 23);

        // Schedule the alarm!
        AlarmManager am = (AlarmManager) context
                .getSystemService(context.ALARM_SERVICE);

        am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender);

//        am.setInexactRepeating(AlarmManager.RTC_WAKEUP,
//                calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, sender);


//         am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, firstTime,
//                 AlarmManager.INTERVAL_DAY, sender);


//        if (sm.isLoggedIn()) {
//            am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, firstTime,
//                    repeating, sender);
//        } else {
//            am.cancel(sender);
//            Log.e("UTILS", "USER NOT LOGGED IN YET");
//        }

    }



    public void setAlarm() {
        final int _id = (int) System.currentTimeMillis();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        // Set the alarm's trigger time to 8:30 a.m.
        calendar.set(Calendar.HOUR_OF_DAY, 4);
        calendar.set(Calendar.MINUTE, 35);
        scheduleAlarm(ScheduleReminders.this, _id, calendar);


        final int _id1 = (int) System.currentTimeMillis();
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTimeInMillis(System.currentTimeMillis());
        // Set the alarm's trigger time to 8:30 a.m.
        calendar2.set(Calendar.HOUR_OF_DAY, 12);
        calendar2.set(Calendar.MINUTE, 36);
        scheduleAlarm(ScheduleReminders.this, 2, calendar2);
    }

    public void setMultipleAlarms(Context context) {


//        AlarmManager mgrAlarm = (AlarmManager) context.getSystemService(ALARM_SERVICE);
//
//        ArrayList<PendingIntent> intentArray = new ArrayList<PendingIntent>();
//
//        for(int i = 0; i < 10; ++i)
//        {
//            Intent intent = new Intent(context, AlaramService.class);
//            // Loop counter `i` is used as a `requestCode`
//            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, i, intent, 0);
//            // Single alarms in 1, 2, ..., 10 minutes (in `i` minutes)
//            mgrAlarm.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
//                    SystemClock.elapsedRealtime() + 10000 * i,
//                    pendingIntent);
//
//            intentArray.add(pendingIntent);
//        }
//
//        AlarmManager[] alarmManager=new AlarmManager[24];
//       // intentArray = new ArrayList<PendingIntent>();
//        for(int f=0;f<arr2.length();f++){
//            Intent intent = new Intent(ScheduleReminders.this, AlaramService.class);
//           PendingIntent pi=PendingIntent.getBroadcast(ScheduleReminders.this, f,intent, 0);
//
//            alarmManager[f] = (AlarmManager) getSystemService(ALARM_SERVICE);
//            alarmManager[f].set(AlarmManager.RTC_WAKEUP,arr2[f] ,pi);
//
//            intentArray.add(pi);
//
//        }


//        for(int i=0;i<5;i++){
//            final int _id = (int) System.currentTimeMillis();
//            Calendar calendar = Calendar.getInstance();
//            calendar.setTimeInMillis(System.currentTimeMillis());
//            // Set the alarm's trigger time to 8:30 a.m.
//            calendar.set(Calendar.HOUR_OF_DAY, 4);
//            calendar.set(Calendar.MINUTE, 13+i);
//
//            scheduleAlarm(ScheduleReminders.this, i, calendar);
//        }

    }

    private void openTimePickerDialog(boolean is24r) {
        Calendar calendar = Calendar.getInstance();

        timePickerDialog = new TimePickerDialog(
                ScheduleReminders.this,
                onTimeSetListener,
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                is24r);
        timePickerDialog.setTitle("Set Alarm Time");

        timePickerDialog.show();

    }

    private void setAlarm(Calendar targetCal) {


        final int RQS_1 = (int) System.currentTimeMillis();
        alarms_status.setText(alarms_status.getText().toString() + "\n" +
                "\n\n***\n"
                + "Alarm is set@ " + targetCal.getTime() + "\n"
                + "***\n");

        Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
        intent.putExtra("alaram_time", "" + targetCal.getTime());
        intent.putExtra("RQS_1", "" + "" + RQS_1);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), RQS_1, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, targetCal.getTimeInMillis(), pendingIntent);


//        insertScheduleRecord(String id, String title, String date, String time, String calleeNumber,
//                String note, String status, String dummy1, String dummy2)
        db.insertScheduleRecord("" + RQS_1, "Schedule - " + RQS_1, Util.getCurrentTime(), "" + targetCal.getTime(), calleNumber,
                note, status, "", "");

    }

}
