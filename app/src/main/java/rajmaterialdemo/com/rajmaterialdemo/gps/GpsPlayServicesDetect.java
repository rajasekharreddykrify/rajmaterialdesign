package rajmaterialdemo.com.rajmaterialdemo.gps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import rajmaterialdemo.com.rajmaterialdemo.R;
import rajmaterialdemo.com.rajmaterialdemo.utils.L;
import rajmaterialdemo.com.rajmaterialdemo.utils.Util;

/**
 * Created by raj on 14/12/15.
 */


public class GpsPlayServicesDetect extends AppCompatActivity {
    public String TAG = "GpsPlayServicesDetect";

    LinearLayout motion_states_ll;

    BroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gps_motion_detect);

        motion_states_ll = (LinearLayout) findViewById(R.id.motion_states_ll);

        Intent myIntent=new Intent(this,PlayLocation.class);
        startService(myIntent);

        addViewsToLayout("Welcome to Gps PlayServices Detect","0");

        receiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                L.i(TAG, "onReceive");
                addViewsToLayout(intent.getStringExtra("msg"),
                        intent.getStringExtra("speed")+
                                "\n old latitude is : "+intent.getStringExtra("oldlat")+
                                "\n old longitude is : "+intent.getStringExtra("oldlon")+
                                "\n new latitude is : "+intent.getStringExtra("newlat")+
                                "\n new longitude is : "+intent.getStringExtra("newlon")+
                                "\n new dist is : "+intent.getStringExtra("dist")+
                                "\n new time is : "+intent.getStringExtra("time")+
                                "\n new speedor is : "+intent.getStringExtra("speedor")+
                                "\n new address is : "+intent.getStringExtra("address")
                );
            }

        };


    }



    public  void addViewsToLayout(String text,String speed) {

        L.i("status", "status is : " + text);
        TextView textView = new TextView(GpsPlayServicesDetect.this);
        textView.setText(text+ "\n" + "speed is : "+speed + "\n" + Util.getCurrentTime()+"\n");
        textView.setTextColor(Color.parseColor("#FFFFFF"));

        ImageView img = new ImageView(GpsPlayServicesDetect.this);
        if(text.equals("Unknown")){
            img.setImageResource(R.drawable.unknown);
        }else if(text.equals("In Vehicle")){
            img.setImageResource(R.drawable.car_driving);
        }else if(text.equals("On Bicycle")){
            img.setImageResource(R.drawable.cycling);
        }else if(text.equals("Running")){
            img.setImageResource(R.drawable.mario_run);
        }else if(text.equals("On Foot")){
            img.setImageResource(R.drawable.onfoot);
        }else if(text.equals("Still")){
            img.setImageResource(R.drawable.still);
        }else if(text.equals("Tilting")){
            img.setImageResource(R.drawable.tilting);
        }else if(text.equals("Walking")){
            img.setImageResource(R.drawable.walk);
        }else{
            img.setImageResource(R.drawable.car_driving);
        }

//        img.getLayoutParams().height = 150;
//        img.getLayoutParams().width = 150;


        motion_states_ll.addView(textView);
        motion_states_ll.addView(img);


    }




    @Override
    protected void onStart() {

        LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
                new IntentFilter("rajmaterialdemo.com.rajmaterialdemo.gps.GpsPlayServicesDetect"));

        super.onStart();
    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onStop();
    }
}
