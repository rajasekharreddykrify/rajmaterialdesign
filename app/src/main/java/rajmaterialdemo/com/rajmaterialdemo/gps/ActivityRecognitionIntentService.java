package rajmaterialdemo.com.rajmaterialdemo.gps;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import rajmaterialdemo.com.rajmaterialdemo.utils.L;

/**
 * Created by raj on 11/12/15.
 */
public class ActivityRecognitionIntentService extends IntentService {
    ActivityRecognitionResult result;
    Intent i;
    DetectedActivity mpactivity;

    public ActivityRecognitionIntentService() {
        super("ActivityRecognitionIntentService");
        i = new Intent("ACTIVITY_RECOGNITION_DATA");
        L.d("ActivityRecognitionIntentService", "ActivityRecognitionIntentService created");
    }



    private String getTypes(int type) {
        L.d("", "type = " + type);
        if(type == DetectedActivity.UNKNOWN)
            return "Unknown";
        else if(type == DetectedActivity.IN_VEHICLE)
            return "In Vehicle";
        else if(type == DetectedActivity.ON_BICYCLE)
            return "On Bicycle";
        else if(type == DetectedActivity.RUNNING)
            return "Running";
        else if(type == DetectedActivity.ON_FOOT)
            return "On Foot";
        else if(type == DetectedActivity.STILL)
            return "Still";
        else if(type == DetectedActivity.TILTING)
            return "Tilting";
        else if(type == DetectedActivity.WALKING)
            return "Walking";
        else
            return "";
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        L.d("onHandleIntent", "onHandleIntent");
        if (intent.getAction() == "ActivityRecognitionIntentService") {
            if(ActivityRecognitionResult.hasResult(intent)){
                result = ActivityRecognitionResult.extractResult(intent);
                mpactivity = result.getMostProbableActivity();
                i.putExtra("Activity", getTypes(mpactivity.getType()));
                i.putExtra("Confidence", mpactivity.getConfidence());
//                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(i);
                this.sendBroadcast(i);
            }
        }
    }



}
