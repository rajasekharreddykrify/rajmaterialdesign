package rajmaterialdemo.com.rajmaterialdemo.gps;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import java.util.List;

import rajmaterialdemo.com.rajmaterialdemo.utils.L;

/**
 * Created by raj on 11/12/15.
 */
public class ActivityBroadcastReceiver extends android.content.BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        L.d("ActivityBroadcastReceiver", "BroadcastReceiver");
//        Toast.makeText(context, "Service received motion is : " + intent.getStringExtra("Activity"), Toast.LENGTH_SHORT).show();


        try {

            ActivityManager mActivityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> RunningTask = mActivityManager.getRunningTasks(1);
            ActivityManager.RunningTaskInfo ar = RunningTask.get(0);

            String activityOnTop = ar.topActivity.getClassName();

            //Toast.makeText(context, "on : "+activityOnTop, Toast.LENGTH_SHORT).show();

            if(activityOnTop.contains("rajmaterialdemo.com.rajmaterialdemo.gps.MotionDetect")){
                L.d("if", "on top");

                Intent ii = new Intent("rajmaterialdemo.com.rajmaterialdemo.gps.MotionDetect");
                ii.putExtra("msg", intent.getStringExtra("Activity"));
                LocalBroadcastManager.getInstance(context).sendBroadcast(ii);



            }else{

                L.d("else", "Background");

            }
        } catch (Exception e) {
            e.printStackTrace();


        }

    }


}