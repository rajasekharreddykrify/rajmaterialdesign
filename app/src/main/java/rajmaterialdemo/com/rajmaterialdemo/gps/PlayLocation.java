package rajmaterialdemo.com.rajmaterialdemo.gps;

import android.app.Service;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import rajmaterialdemo.com.rajmaterialdemo.utils.L;

public class PlayLocation extends Service implements ConnectionCallbacks,
		OnConnectionFailedListener, LocationListener {
	private static final String TAG = PlayLocation.class.getSimpleName();

	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

	private Location mLastLocation;

	// Google client to interact with Google API
	private GoogleApiClient mGoogleApiClient;

	// boolean flag to toggle periodic location updates
	private boolean mRequestingLocationUpdates = true;

	private LocationRequest mLocationRequest;

	// Location updates intervals in sec
	private static int UPDATE_INTERVAL = 10000; // 10 sec
	private static int FATEST_INTERVAL = 5000; // 5 sec
	private static int DISPLACEMENT = 1; // 10 meters
	public static String playlat = "0.0";
	public static String playlog = "0.0";



	double lat_old=0.0;
	double lon_old=0.0;
	double lat_new;
	double lon_new;
	double time=5;
	double speed=0.0;
	int sp =0;


	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		if (checkPlayServices()) {

			// Building the GoogleApi client
			buildGoogleApiClient();

			createLocationRequest();
		}

		if (mGoogleApiClient != null) {
			mGoogleApiClient.connect();
		}
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
				+ result.getErrorCode());
	}

	@Override
	public void onConnected(Bundle arg0) {

		// Once connected with google api, get the location
		displayLocation();

		if (mRequestingLocationUpdates) {
			startLocationUpdates();
		}
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		mGoogleApiClient.connect();
	}

	@Override
	public void onLocationChanged(Location location) {
		// Assign the new location

		if(mLastLocation!=null){
			lat_old=mLastLocation.getLatitude();
			lon_old=mLastLocation.getLongitude();
		}
		mLastLocation = location;


		lat_new = location.getLatitude();
		lon_new = location.getLongitude();

//		 Toast.makeText(getApplicationContext(), "Location changed!",
//		 Toast.LENGTH_SHORT).show();

		sp = Integer.parseInt(sep(String.valueOf((calculateDistance(lat_new, lon_new, lat_old, lon_old))/time)));


//		sp = Integer.parseInt(sep(String.valueOf(location.getSpeed())));

		//sp = Integer.parseInt(sep(String.valueOf((getDistance(lat_new, lon_new, lat_old, lon_old))/time)));



		Log.i("old", "latitude is : " + lat_old  );
		Log.i("old", "longitude is : " + lon_old);
		Log.i("new", "latitude is : " + location.getLongitude());
		Log.i("new", "longitude is : " + location.getLatitude());
		Log.i("time", "time is : " + time);
		Log.i("distance", "distance is : " + getDistance(lat_new, lon_new, lat_old, lon_old));
		Log.i("speed", "speed is : " + sp);


		L.i("sp","sp is : "+sp);
		if(sp==0) {
			sendBroadcastNow(0,sp);
		}else if(sp>=1 && sp<5){
			sendBroadcastNow(1,sp);
		}else if(sp>=5 && sp<10){
			sendBroadcastNow(5,sp);
		}else if(sp>=10 && sp<20){
			sendBroadcastNow(10,sp);
		}else if(sp>=20 && sp<30){
			sendBroadcastNow(20,sp);
		}else if(sp>=30){
			sendBroadcastNow(30,sp);
		}else{
			sendBroadcastNow(30,sp);
		}

		lat_old=mLastLocation.getLatitude();
		lon_old=mLastLocation.getLongitude();

		// Displaying the new location on UI
		displayLocation();
	}

	private void displayLocation() {

		mLastLocation = LocationServices.FusedLocationApi
				.getLastLocation(mGoogleApiClient);

		if (mLastLocation != null) {
			double latitude = mLastLocation.getLatitude();
			double longitude = mLastLocation.getLongitude();

			Log.e("Lat LOG", latitude + ", " + longitude);
			playlat = "" + latitude;
			playlog = "" + longitude;

		} else {

		}
	}

	/**
	 * Method to toggle periodic location updates
	 * */
	private void togglePeriodicLocationUpdates() {
		if (!mRequestingLocationUpdates) {
			// Changing the button text

			mRequestingLocationUpdates = true;

			// Starting the location updates
			startLocationUpdates();

			Log.d(TAG, "Periodic location updates started!");

		} else {
			// Changing the button text

			mRequestingLocationUpdates = false;

			// Stopping the location updates
			stopLocationUpdates();

			Log.d(TAG, "Periodic location updates stopped!");
		}
	}

	/**
	 * Creating google api client object
	 * */
	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API).build();
	}

	/**
	 * Creating location request object
	 * */
	protected void createLocationRequest() {
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(UPDATE_INTERVAL);
		mLocationRequest.setFastestInterval(FATEST_INTERVAL);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
	}

	/**
	 * Method to verify google play services on the device
	 * */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				// GooglePlayServicesUtil.getErrorDialog(resultCode,getApplicationContext(),
				// PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				// Toast.makeText(getApplicationContext(),
				// "This device is not supported.", Toast.LENGTH_LONG)
				// .show();
				// finish();
			}
			return false;
		}
		return true;
	}

	/**
	 * Starting the location updates
	 * */
	protected void startLocationUpdates() {

		LocationServices.FusedLocationApi.requestLocationUpdates(
				mGoogleApiClient, mLocationRequest, this);

	}

	/**
	 * Stopping location updates
	 */
	protected void stopLocationUpdates() {
		LocationServices.FusedLocationApi.removeLocationUpdates(
				mGoogleApiClient, this);
	}

	@Override
	public boolean stopService(Intent name) {
		// TODO Auto-generated method stub
		this.stopSelf();
		return super.stopService(name);
	}





	private String getTypes(int type) {
		L.d("", "type = " + type);
		if(type == 0)
			return "On Foot";
		else if(type == 0)
			return "Still";
		else if(type == 1)
			return "Tilting";
		else if(type == 5)
			return "Walking";
		else if(type == 10)
			return "Running";
		else if(type == 20)
			return "In Vehicle";
       /* else if(type == DetectedActivity.ON_BICYCLE)
            return "On Bicycle";*/

        /*else if(type == 0)
            return "On Foot";*/

		else
			return "";
	}


	public void sendBroadcastNow(int val,int sp){

		L.i("GPS SERVICE","sendBroadcastNow");
		Intent ii = new Intent("rajmaterialdemo.com.rajmaterialdemo.gps.GpsPlayServicesDetect");
		ii.putExtra("msg", getTypes(val));
		ii.putExtra("speed",   ""+sp);
		ii.putExtra("oldlat",   ""+lat_old);
		ii.putExtra("oldlon",   ""+lon_old);
		ii.putExtra("newlat",   ""+lat_new);
		ii.putExtra("newlon",   ""+lon_new);
		ii.putExtra("dist",   ""+getDistance(lat_new, lon_new, lat_old, lon_old));
		ii.putExtra("time",   ""+time);
		ii.putExtra("address",   ""+getAddress(lat_new, lon_new));
		ii.putExtra("speedor",   ""+(getDistance(lat_new, lon_new, lat_old, lon_old))/time);
		LocalBroadcastManager.getInstance(this).sendBroadcast(ii);



	}

	public static String sep(String s)
	{
		L.i("sep","sep string is : "+s);
		int l = s.indexOf(".");
		if (l >0)
		{
			return s.substring(0, l);
		}
		return "";

	}


	/**
	 * Gets distance in meters, coordinates in RADIAN
	 */
	private static double getDistance(double lat1, double lon1, double lat2, double lon2) {


//        double R = 6371000; // for haversine use R = 6372.8 km instead of 6371 km
		double R = 6372.8; // for haversine use R = 6372.8 km instead of 6371 km
		double dLat = lat2 - lat1;
		double dLon = lon2 - lon1;
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
				Math.cos(lat1) * Math.cos(lat2) *
						Math.sin(dLon / 2) * Math.sin(dLon / 2);
		//double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

		L.i("getDistance", "getDistance is  : " + 2 * R * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)));
		return 2 * R * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		// simplify haversine:
		//return 2 * R * 1000 * Math.asin(Math.sqrt(a));
	}

	private static long calculateDistance(double lat1, double lng1, double lat2, double lng2) {
		double dLat = Math.toRadians(lat2 - lat1);
		double dLon = Math.toRadians(lng2 - lng1);
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
				+ Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
				* Math.sin(dLon / 2);
		double c = 2 * Math.asin(Math.sqrt(a));
		long distanceInMeters = Math.round(6371000 * c);

		L.i("distanceInMeters", "distanceInMeters is : " + distanceInMeters);
		return distanceInMeters;
	}


	public String getAddress(double lat, double lng) {

		String result = "";
		Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
		try {
			List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
			Address obj = addresses.get(0);
			String add = obj.getAddressLine(0);
//			GUIStatics.currentAddress = obj.getSubAdminArea() + ","
//					+ obj.getAdminArea();
//			GUIStatics.latitude = obj.getLatitude();
//			GUIStatics.longitude = obj.getLongitude();
//			GUIStatics.currentCity= obj.getSubAdminArea();
//			GUIStatics.currentState= obj.getAdminArea();
			add = add + "\n" + obj.getCountryName();
			add = add + "\n" + obj.getCountryCode();
			add = add + "\n" + obj.getAdminArea();
			add = add + "\n" + obj.getPostalCode();
			add = add + "\n" + obj.getSubAdminArea();
			add = add + "\n" + obj.getLocality();
			add = add + "\n" + obj.getSubThoroughfare();

			result = add;

			Log.v("IGA", "Address" + add);
			// Toast.makeText(this, "Address=>" + add,
			// Toast.LENGTH_SHORT).show();

			// TennisAppActivity.showDialog(add);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
		}

		return result;
	}
}