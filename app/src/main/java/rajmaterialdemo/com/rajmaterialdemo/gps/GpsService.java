package rajmaterialdemo.com.rajmaterialdemo.gps;

/**
 * Created by raj on 11/12/15.
 */

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.DetectedActivity;

import java.text.DecimalFormat;

import rajmaterialdemo.com.rajmaterialdemo.Utilities;
import rajmaterialdemo.com.rajmaterialdemo.utils.L;

public class GpsService extends Service
{
    private LocationManager locManager;
    private LocationListener locListener = new myLocationListener();
    static final Double EARTH_RADIUS = 6371.00;

    private boolean gps_enabled = false;
    private boolean network_enabled = false;

    int i =0;
//    double sp=0;
    int sp =0;



    double lat_old=0.0;
    double lon_old=0.0;
    double lat_new;
    double lon_new;
    double time=10;
    double speed=0.0;

    private Handler handler = new Handler();
    Thread t;

    @Override
    public IBinder onBind(Intent intent) {return null;}
    @Override
    public void onCreate() {}
    @Override
    public void onDestroy() {}
    @Override
    public void onStart(Intent intent, int startid) {}
    @Override
    public int onStartCommand(Intent intent, int flags, int startId){

        Toast.makeText(getBaseContext(), "Service Started", Toast.LENGTH_SHORT).show();

        final Runnable r = new Runnable()
        {   public void run()
            {
                Log.v("Debug", "Hello");
                location();
                handler.postDelayed(this, 5000);
            }
        };
        handler.postDelayed(r, 5000);
        return START_STICKY;
    }

    public void location(){
        locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        try{
            gps_enabled = locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }
        catch(Exception ex){}
        try{
            network_enabled = locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }
        catch(Exception ex){}
        Log.v("Debug", "in on create.. 2");
        if (gps_enabled) {
            locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,locListener);
            Log.v("Debug", "gps Enabled..");
        }
        if (network_enabled) {
            locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,0,0,locListener);
            Log.v("Debug", "network Disabled..");
        }
        Log.v("Debug", "in on create..3");
    }

    private class myLocationListener implements LocationListener
    {
//        double lat_old=0.0;
//        double lon_old=0.0;
//        double lat_new;
//        double lon_new;
//        double time=10;
//        double speed=0.0;

//        double c_lat_new = 17.001496;
//        double c_lng_new = 82.242197;


        @Override
        public void onLocationChanged(Location location) {
            Log.v("Debug", "in onLocation changed..");
            if(location!=null){
                locManager.removeUpdates(locListener);
                String Speed = "Device Speed: " +location.getSpeed();
                lat_new=location.getLongitude();
                lon_new =location.getLatitude();
                String longitude = "Longitude: " +location.getLongitude();
                String latitude = "Latitude: " +location.getLatitude();

                sp = Integer.parseInt(sep(String.valueOf((calculateDistance(lat_new, lon_new, lat_old, lon_old))/time)));



                Log.i("old", "latitude is : " + lat_old  );
                Log.i("old", "longitude is : " + lon_old);
                Log.i("new", "latitude is : " + location.getLongitude());
                Log.i("new", "longitude is : " + location.getLatitude());
                Log.i("time", "time is : " + time);
                Log.i("distance", "distance is : " + calculateDistance(lat_new, lon_new, lat_old, lon_old));
                Log.i("speed", "speed is : " + sp);

//                Log.i("speed", "update is : " + longitude + "\n" + latitude +    "\nSpeed is: " + Speed);
//                Log.d("speed", "update is : " + longitude + "\n" + latitude +   "\nSpeed is: " + Speed);
//                Log.e("speed", "update is : " + longitude + "\n" + latitude +   "\nSpeed is: " + Speed);
//
//                Toast.makeText(getBaseContext(), "Current speed:" + location.getSpeed(),
//                        Toast.LENGTH_SHORT).show();
//                double distance =CalculationByDistance(lat_new, lon_new, lat_old, lon_old);


//                speed = distance/time;

//                Log.i("speed", "update is : " + longitude + "\n" + latitude + "\nDistance is: "
//                        + distance + "\nSpeed is: " + speed);
//                Log.d("speed", "update is : " + longitude + "\n" + latitude + "\nDistance is: "
//                        + distance + "\nSpeed is: " + speed);
//                Log.e("speed", "update is : " + longitude + "\n" + latitude + "\nDistance is: "
//                        + distance + "\nSpeed is: " + speed);


//                int sp = (int)Math.round(speed);
//                int sp = Integer.parseInt(String.valueOf(speed));
//                Double d = new Double(speed);
//                int sp = d.intValue();
//                sp = speed;

//                  sp = (int)Math.floor(speed + 0.5d);

//                 sp = Integer.parseInt(df2.format(speed));
//                double num;
//                long iPart;
//                double fPart;
//
//                num = speed;
//                iPart = (long) num;
//
//                fPart = num - iPart;
//
//                System.out.println("Integer part = " + iPart);
//                System.out.println("Fractional part = " + fPart);

//                String speedStr=""+speed;
//
//                L.i("speed str","speed str is : "+sep(""+speedStr));
//                L.e("speed str", "speed str is : " + sep("" + speedStr));
//                L.d("speed str", "speed str is : " + sep("" + speedStr));

//                speedStr.

//                String subSpeed= speedStr.substring(0,)


//                sp = safeLongToInt(iPart);


//                c_lat_new = c_lng_new + 1.0;
//                c_lng_new = c_lng_new + 1.0;
//                double s = calc(lat_new, lon_new, lat_old, lon_old);
//                double s = calc((c_lat_new ), (c_lng_new ), lat_old, lon_old);
//                double t = s/time;
//                L.i("location distance","distance is : "+s);
//                L.i("location time","time is : "+time);
//                L.i("location speed","speed is : "+t);
//                L.i("location speed","sep is : "+sep(String.valueOf(t)));


              //  sp = Integer.parseInt(sep(String.valueOf((getDistance(lat_new, lon_new, lat_old, lon_old))/time)));
                sp = Integer.parseInt(sep(String.valueOf((calculateDistance(lat_new, lon_new, lat_old, lon_old))/time)));


//                sp =Integer.parseInt(sep(String.valueOf(location.getSpeed())))+(Utilities.getCurrentSeconds());
               // sp =Integer.parseInt(sep(String.valueOf(location.getSpeed())));

//                sp = Integer.parseInt(sep(t+""));

                L.i("sp","sp is : "+sp);
                if(sp==0) {
                    sendBroadcastNow(0,sp);
                }else if(sp>=1 && sp<5){
                    sendBroadcastNow(1,sp);
                }else if(sp>=5 && sp<10){
                    sendBroadcastNow(5,sp);
                }else if(sp>=10 && sp<20){
                    sendBroadcastNow(10,sp);
                }else if(sp>=20 && sp<30){
                    sendBroadcastNow(20,sp);
                }else if(sp>=30){
                    sendBroadcastNow(30,sp);
                }else{
                    sendBroadcastNow(30,sp);
                }


                /*Toast.makeText(getApplicationContext(), longitude+"\n"+latitude+"\nDistance is: "
                        +distance+"\nSpeed is: "+speed +"\nSp is: "+sp , Toast.LENGTH_SHORT).show();*/
                lat_old=lat_new;
                lon_old=lon_new;
            }
        }
        @Override
        public void onProviderDisabled(String provider) {}
        @Override
        public void onProviderEnabled(String provider) {}
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    }

    public double CalculationByDistance(double lat1, double lon1, double lat2, double lon2) {
        double Radius = EARTH_RADIUS;
        double dLat = Math.toRadians(lat2-lat1);
        double dLon = Math.toRadians(lon2-lon1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLon/2) * Math.sin(dLon/2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return Radius * c;
    }




    private String getTypes(int type) {
        L.d("", "type = " + type);
        if(type == 0)
            return "On Foot";
        else if(type == 0)
            return "Still";
        else if(type == 1)
            return "Tilting";
        else if(type == 5)
            return "Walking";
        else if(type == 10)
            return "Running";
        else if(type == 20)
            return "In Vehicle";
       /* else if(type == DetectedActivity.ON_BICYCLE)
            return "On Bicycle";*/

        /*else if(type == 0)
            return "On Foot";*/

        else
            return "";
    }


    public void sendBroadcastNow(int val,int sp){

        L.i("GPS SERVICE","sendBroadcastNow");
        Intent ii = new Intent("rajmaterialdemo.com.rajmaterialdemo.gps.GpsMotionDetect");
        ii.putExtra("msg", getTypes(val));
        ii.putExtra("speed",   ""+sp);
        ii.putExtra("oldlat",   ""+lat_old);
        ii.putExtra("oldlon",   ""+lon_old);
        ii.putExtra("newlat",   ""+lat_new);
        ii.putExtra("newlon",   ""+lon_new);
        ii.putExtra("dist",   ""+calculateDistance(lat_new, lon_new, lat_old, lon_old));
        ii.putExtra("time",   ""+time);
        ii.putExtra("speedor",   ""+(calculateDistance(lat_new, lon_new, lat_old, lon_old))/time);
        LocalBroadcastManager.getInstance(this).sendBroadcast(ii);



    }

    public static int safeLongToInt(long l) {
        if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
            throw new IllegalArgumentException
                    (l + " cannot be cast to int without changing its value.");
        }
        return (int) l;
    }



    public static String sep(String s)
    {
        L.i("sep","sep string is : "+s);
        int l = s.indexOf(".");
        if (l >0)
        {
            return s.substring(0, l);
        }
        return "";

    }


    public static float calc(double latA,double lngA,double latB,double lngB){
        Location locationA = new Location("point A");
        locationA.setLatitude(latA);
        locationA.setLongitude(lngA);
        Location locationB = new Location("point B");
        locationB.setLatitude(latB);
        locationB.setLongitude(lngB);




        return locationA.distanceTo(locationB) ;
    }


    /**
     * Gets distance in meters, coordinates in RADIAN
     */
    private static double getDistance1(double lat1, double lon1, double lat2, double lon2) {


//        double R = 6371000; // for haversine use R = 6372.8 km instead of 6371 km
        double R = 6372.8; // for haversine use R = 6372.8 km instead of 6371 km
        double dLat = lat2 - lat1;
        double dLon = lon2 - lon1;
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(lat1) * Math.cos(lat2) *
                        Math.sin(dLon / 2) * Math.sin(dLon / 2);
        //double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        L.i("getDistance", "getDistance is  : " + 2 * R * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)));
        return 2 * R * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        // simplify haversine:
        //return 2 * R * 1000 * Math.asin(Math.sqrt(a));
    }


    private static long calculateDistance(double lat1, double lng1, double lat2, double lng2) {
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        long distanceInMeters = Math.round(6371000 * c);

        L.i("distanceInMeters","distanceInMeters is : "+distanceInMeters);
        return distanceInMeters;
    }
}
