package rajmaterialdemo.com.rajmaterialdemo.gps;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.ActivityRecognitionApi;

import rajmaterialdemo.com.rajmaterialdemo.R;
import rajmaterialdemo.com.rajmaterialdemo.utils.L;
import rajmaterialdemo.com.rajmaterialdemo.utils.Util;

/**
 * Created by raj on 11/12/15.
 */
public class MotionDetect extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks , GoogleApiClient.OnConnectionFailedListener  {

    public String TAG = "MotionDetect";
    GoogleApiClient gac;

    LinearLayout motion_states_ll;

    BroadcastReceiver receiver;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.motion_detect);



        motion_states_ll = (LinearLayout) findViewById(R.id.motion_states_ll);

        gac =  new GoogleApiClient.Builder(getApplicationContext())
                .addApi(ActivityRecognition.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        gac.connect();

        addViewsToLayout("Welcome to Motion Detect");

        receiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                L.i(TAG,"onReceive");
                addViewsToLayout(intent.getStringExtra("msg"));
            }

        };








    }


    @Override
    public void onConnected(Bundle bundle) {

//        Intent i = new Intent(this, ActivityRecognitionIntentService.class);
//        PendingIntent mActivityRecognitionPendingIntent = PendingIntent.getService(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
        L.i("gac", "onConnected");
//        ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(gac,0,mActivityRecognitionPendingIntent);
        Intent i = new Intent(this, ActivityRecognitionIntentService.class);
        i.setAction("ActivityRecognitionIntentService");
        PendingIntent activityRecognitionPendingIntent = PendingIntent.getService(this, 7422, i, PendingIntent.FLAG_UPDATE_CURRENT);
        ActivityRecognitionApi rec = ActivityRecognition.ActivityRecognitionApi;
        rec.requestActivityUpdates(gac, 5000, activityRecognitionPendingIntent)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        if (status.isSuccess()) {
                            L.i(TAG, "Successfully registered updates");
                        } else {
                            L.i(TAG, "Failed to register updates");
                        }
                    }
                });

    }

    @Override
    public void onConnectionSuspended(int i) {
        L.i("gac", "onConnectionSuspended");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        L.i("gac", "onConnectionFailed");

    }

    public  void addViewsToLayout(String text) {

        L.i("status", "status is : " + text);
        TextView textView = new TextView(MotionDetect.this);
        textView.setText(text+ "\n" + Util.getCurrentTime()+"\n");
        textView.setTextColor(Color.parseColor("#FFFFFF"));

        ImageView img = new ImageView(MotionDetect.this);
        if(text.equals("Unknown")){
            img.setImageResource(R.drawable.unknown);
        }else if(text.equals("In Vehicle")){
            img.setImageResource(R.drawable.car_driving);
        }else if(text.equals("On Bicycle")){
            img.setImageResource(R.drawable.cycling);
        }else if(text.equals("Running")){
            img.setImageResource(R.drawable.mario_run);
        }else if(text.equals("On Foot")){
            img.setImageResource(R.drawable.onfoot);
        }else if(text.equals("Still")){
            img.setImageResource(R.drawable.still);
        }else if(text.equals("Tilting")){
            img.setImageResource(R.drawable.tilting);
        }else{
            img.setImageResource(R.drawable.unknown);
        }

//        img.getLayoutParams().height = 150;
//        img.getLayoutParams().width = 150;


        motion_states_ll.addView(textView);
        motion_states_ll.addView(img);
    }


    @Override
    protected void onStart() {

        LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
                new IntentFilter("rajmaterialdemo.com.rajmaterialdemo.gps.MotionDetect"));

        super.onStart();
    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onStop();
    }


}
