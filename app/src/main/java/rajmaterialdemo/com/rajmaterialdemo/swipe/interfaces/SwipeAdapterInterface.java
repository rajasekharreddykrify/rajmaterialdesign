package rajmaterialdemo.com.rajmaterialdemo.swipe.interfaces;

public interface SwipeAdapterInterface {

    int getSwipeLayoutResourceId(int position);

    void notifyDatasetChanged();

}
