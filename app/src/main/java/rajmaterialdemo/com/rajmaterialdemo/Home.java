package rajmaterialdemo.com.rajmaterialdemo;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import rajmaterialdemo.com.rajmaterialdemo.asterisk.TestDefaultManagerConnection;
import rajmaterialdemo.com.rajmaterialdemo.db.RajDB;
import rajmaterialdemo.com.rajmaterialdemo.gps.GpsMotionDetect;
import rajmaterialdemo.com.rajmaterialdemo.gps.GpsPlayServicesDetect;
import rajmaterialdemo.com.rajmaterialdemo.gps.GpsService;
import rajmaterialdemo.com.rajmaterialdemo.gps.PlayLocation;
import rajmaterialdemo.com.rajmaterialdemo.pathsense.PathSenseActivity;
import rajmaterialdemo.com.rajmaterialdemo.receivers.ApplicationReceiver;
import rajmaterialdemo.com.rajmaterialdemo.receivers.ForegroundBackgroundReceiver;
import rajmaterialdemo.com.rajmaterialdemo.services.NotificationBlockUnblockService;
import rajmaterialdemo.com.rajmaterialdemo.sidemenu.API23;
import rajmaterialdemo.com.rajmaterialdemo.sidemenu.NativeContentProvider;
import rajmaterialdemo.com.rajmaterialdemo.sidemenu.Svg;
import rajmaterialdemo.com.rajmaterialdemo.sidemenu.SwipeLeftRightActivity;
import rajmaterialdemo.com.rajmaterialdemo.sidemenu.ViewPagerWithImagesTutorials;
import rajmaterialdemo.com.rajmaterialdemo.swipestack.SwipeCards;
import rajmaterialdemo.com.rajmaterialdemo.teams.Android;
import rajmaterialdemo.com.rajmaterialdemo.teams.IOS;
import rajmaterialdemo.com.rajmaterialdemo.teams.WindowsFragment;
import rajmaterialdemo.com.rajmaterialdemo.user.DeviceDetailsDemo;
import rajmaterialdemo.com.rajmaterialdemo.user.Register;
import rajmaterialdemo.com.rajmaterialdemo.utils.L;
import rajmaterialdemo.com.rajmaterialdemo.utils.Show;

public class Home extends AppCompatActivity {

    ViewPager mViewPager;
    FragmentPagerAdapter adapterViewPager;

    Toolbar toolbar ;
    DrawerLayout dlDrawer;
    NavigationView nvDrawer;
    ActionBarDrawerToggle drawerToggle;

    RajDB db;

    Intent in ;

    TabLayout tabs;
    private int[] tabIcons = {
            R.drawable.ic_cart,
            R.drawable.ic_cart,
            R.drawable.ic_cart
    };

//    ArrayAdapter<String> myAdapter;
//    ListView listView;
//    String[] dataArray = new String[] {"India","Androidhub4you", "Pakistan", "Srilanka", "Nepal", "Japan"};



    private SearchView mSearchView;
    private MenuItem searchMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_raj_material_demo);

//        Intent in = new Intent(Home.this, ImageBlurDownload.class);
//        startActivity(in);

//        Intent in = new Intent(Home.this, ActivityLifecycle.class);
//        startActivity(in);


//        hideAppIcon();
//        Intent myIntent=new Intent(this,PlayLocation.class);
//        startService(myIntent);

//        Intent intent=new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
//        Intent intent=new Intent(Home.this, NotificationBlockUnblockService.class);
//       startService(intent);


        
        
        
        
        
        
        
        
//        listView = (ListView) findViewById(R.id.listview);
//        myAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, dataArray);
//        listView.setAdapter(myAdapter);
//        listView.setTextFilterEnabled(true);

        registerApplicationReceiver();

        registerForegroundBackgroundReceiver();

        db = new RajDB(Home.this);

        toolbar  = (Toolbar) findViewById(R.id.toolbar);
        setToolbar();

        dlDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = setupDrawerToggle();



        dlDrawer.setDrawerListener(drawerToggle);


        // Find our drawer view
        nvDrawer = (NavigationView) findViewById(R.id.nvView);
        // Setup drawer view
        setupDrawerContent(nvDrawer);


        mViewPager = (ViewPager) findViewById(R.id.pager);


        adapterViewPager = new MyPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(adapterViewPager);



        // Attach the page change listener inside the activity
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            // This method will be invoked when a new page becomes selected.
            @Override
            public void onPageSelected(int position) {

                invalidateOptionsMenu();
                Snackbar.make(mViewPager, "Selected page position: " + position, Snackbar.LENGTH_LONG).show();
            }

            // This method will be invoked when the current page is scrolled
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // Code goes here
            }

            // Called when the scroll state changes:
            // SCROLL_STATE_IDLE, SCROLL_STATE_DRAGGING, SCROLL_STATE_SETTLING
            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });



          tabs = (TabLayout) findViewById(R.id.tabs);

        tabs.setFitsSystemWindows(true);
        tabs.setupWithViewPager(mViewPager);
        setupTabIcons();


//        ASTERISK
        TestDefaultManagerConnection con = new TestDefaultManagerConnection();

        try {
            con.testLogin();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private boolean isNLServiceRunning() {
        try {
            ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);

            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (NotificationBlockUnblockService.class.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }

        return false;
    }

    public void registerApplicationReceiver(){
        ApplicationReceiver ar = new ApplicationReceiver();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
        intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        intentFilter.addAction(Intent.ACTION_PACKAGE_RESTARTED);
        intentFilter.addDataScheme("package");
        registerReceiver(ar, intentFilter);
    }

    public void registerForegroundBackgroundReceiver(){
        ForegroundBackgroundReceiver ar = new ForegroundBackgroundReceiver();

        IntentFilter intentFilter = new IntentFilter();

        intentFilter.addAction(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        intentFilter.addDataScheme("background");
        registerReceiver(ar, intentFilter);
    }

    public void hideAppIcon(){
        PackageManager p = getPackageManager();
        p.setComponentEnabledSetting(getComponentName(),
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }

    private void setupTabIcons() {
        tabs.getTabAt(0).setIcon(tabIcons[0]);
        tabs.getTabAt(1).setIcon(tabIcons[1]);
        tabs.getTabAt(2).setIcon(tabIcons[2]);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, dlDrawer, toolbar, R.string.drawer_open, R.string.drawer_close);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the planet to show based on
        // position
        Fragment fragment = null;

        Class fragmentClass;
        switch(menuItem.getItemId()) {
            case R.id.nav_first_fragment:

                Intent in = new Intent(Home.this,Register.class);
                startActivity(in);

                break;
            case R.id.nav_second_fragment:

//                Intent in1 = new Intent(Home.this,UserContacts.class);
//                Intent in1 = new Intent(Home.this,DeviceContacts.class);
//                Intent in1 = new Intent(Home.this,ScheduleList.class);
//                Intent in1 = new Intent(Home.this,ScheduleReminders.class);
                Intent in1 = new Intent(Home.this,NativeContentProvider.class);
                startActivity(in1);

                break;
            case R.id.nav_third_fragment:

                Intent in2 = new Intent(Home.this,PathSenseActivity.class);
                startActivity(in2);

                break;

            case R.id.nav_forth_fragment:

                Intent in3 = new Intent(Home.this,GpsMotionDetect.class);
                startActivity(in3);


                break;

            case R.id.nav_fifth_fragment:

                Intent in4 = new Intent(Home.this,GpsPlayServicesDetect.class);
                startActivity(in4);


                break;

            case R.id.nav_sixth_fragment:


                Intent in5 = new Intent(Home.this,DeviceDetailsDemo.class);
                startActivity(in5);


                break;

            case R.id.nav_seventh_fragment:


                Intent in6 = new Intent(Home.this,Svg.class);
                startActivity(in6);


                break;
            case R.id.nav_eight_fragment:


                Intent in7 = new Intent(Home.this,API23.class);
                startActivity(in7);


                break;


            case R.id.nav_ninth_fragment:

//                sendSMTPEmail();
//                Intent in8 = new Intent(Home.this,DeviceDetails.class);
//                startActivity(in8);


                break;

            case R.id.nav_tenth_fragment:

                Intent in8 = new Intent(Home.this,SwipeCards.class);
                startActivity(in8);


                break;

            case R.id.nav_eleventh_fragment:

                Intent in9 = new Intent(Home.this,SwipeLeftRightActivity.class);
                startActivity(in9);


                break;

            case R.id.nav_twelve_fragment:

                Intent in12 = new Intent(Home.this,ViewPagerWithImagesTutorials.class);
                startActivity(in12);


                break;
            default:
                mViewPager.setCurrentItem(0);
                //fragmentClass = Android.class;
        }

//        try {
//            fragment = (Fragment) fragmentClass.newInstance();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        // Insert the fragment by replacing any existing fragment
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

        // Highlight the selected item, update the title, and close the drawer
        menuItem.setChecked(true);
        setTitle(menuItem.getTitle());
        dlDrawer.closeDrawers();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.


        getMenuInflater().inflate(R.menu.menu_raj_material_demo, menu);
        MenuItem item = menu.findItem(R.id.action_shop);
        // Obtener drawable del item
        LayerDrawable icon = (LayerDrawable) item.getIcon();

        // Actualizar el contador
        Utilities.setBadgeCount(this, icon, 3);


        getMenuInflater().inflate(R.menu.menu_main, menu);
        searchMenuItem = menu.findItem(R.id.menu_search);
        mSearchView = (SearchView) searchMenuItem.getActionView();
        mSearchView.setOnQueryTextListener(listener);


//        SearchManager searchManager =
//                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        SearchView searchView =
//                (SearchView) menu.findItem(R.id.menu_search).getActionView();
//        searchView.setSearchableInfo(
//                searchManager.getSearchableInfo(getComponentName()));



/*

        //working

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);

        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextChange(String newText)
            {
                // this is your adapter that will be filtered
                myAdapter.getFilter().filter(newText);
                System.out.println("on text chnge text: "+newText);
                return true;
            }
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                // this is your adapter that will be filtered
                myAdapter.getFilter().filter(query);
                System.out.println("on query submit: "+query);
                return true;
            }
        };
        searchView.setOnQueryTextListener(textChangeListener);


*/











        return true;
    }






    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.item_one) {

            Show.displayToast(Home.this,"ONE");
            return true;
        }
        if (id == R.id.item_two) {

            Show.displayToast(Home.this,"TWO");
            return true;
        }

        if (id == R.id.item_three) {

            Show.displayToast(Home.this,"THREE");
            return true;
        }

        if (id == R.id.item_four) {

            Show.displayToast(Home.this,"FOUR");
            return true;
        }
        if (id == R.id.item_five) {

            Show.displayToast(Home.this, "FIVE");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    public boolean onPrepareOptionsMenu(Menu menu) {
        int pageNum = mViewPager.getCurrentItem();
        if(pageNum== 0){
            menu.findItem(R.id.action_shop).setVisible(true);
            menu.findItem(R.id.action_app).setVisible(false);

        }else if(pageNum== 1){

            menu.findItem(R.id.action_shop).setVisible(false);
            menu.findItem(R.id.action_app).setVisible(true);

            menu.findItem(R.id.item_one).setVisible(false);
            menu.findItem(R.id.item_two).setVisible(false);

        }else if(pageNum== 2){
            menu.findItem(R.id.action_shop).setVisible(true);
            menu.findItem(R.id.action_app).setVisible(true);
            menu.findItem(R.id.action_shop).setVisible(false);
            menu.findItem(R.id.item_three).setVisible(false);
            menu.findItem(R.id.item_four).setVisible(false);
        }
        return  true;
    }



    private void setToolbar() {


        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {

            ab.setHomeAsUpIndicator(R.drawable.ic_menu);
            ab.setDisplayHomeAsUpEnabled(true);
        }

    }



    public  class MyPagerAdapter extends FragmentPagerAdapter {
        private  int NUM_ITEMS = 3;

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: // Fragment # 0 - This will show FirstFragment
                    return Android.newInstance(0,  getString(R.string.android));
                case 1: // Fragment # 1 - This will show SecondFragment different title
                    return IOS.newInstance(1, getString(R.string.ios));
                case 2: // Fragment # 2 - This will show ThirdFragment
                    return WindowsFragment.newInstance(2, getString(R.string.windows));
//                case 3: // Fragment # 0 - This will show FirstFragment
//                    return Android.newInstance(3,  getString(R.string.android));
//                case 4: // Fragment # 1 - This will show SecondFragment different title
//                    return IOS.newInstance(4, getString(R.string.ios));
//                case 5: // Fragment # 2 - This will show ThirdFragment
//                    return WindowsFragment.newInstance(5, getString(R.string.windows));
                default:
                    return null;
            }
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0: // Fragment # 0 - This will show FirstFragment
                    return   getString(R.string.android);
                case 1: // Fragment # 1 - This will show SecondFragment different title
                    return getString(R.string.ios);
                case 2: // Fragment # 2 - This will show ThirdFragment
                    return   getString(R.string.windows);
//                case 3: // Fragment # 0 - This will show FirstFragment
//                    return   getString(R.string.android);
//                case 4: // Fragment # 1 - This will show SecondFragment different title
//                    return getString(R.string.ios);
//                case 5: // Fragment # 2 - This will show ThirdFragment
//                    return   getString(R.string.windows);
                default:
                    return "";
            }


        }

    }


    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();

        L.i("Home","onResume");

        Intent myIn=new Intent(this,GpsService.class);
        stopService(myIn);



//
        Intent myIntent=new Intent(this,PlayLocation.class);
        stopService(myIntent);



        Toast.makeText(Home.this, "NotificationBlockUnblockService is "+isNLServiceRunning(), Toast.LENGTH_SHORT).show();



        if(isNLServiceRunning()){
            Toast.makeText(Home.this, "NotificationBlockUnblockService is "+isNLServiceRunning(), Toast.LENGTH_SHORT).show();

        }else{
            Intent intent=new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
            startActivity(intent);
        }

    }



    SearchView.OnQueryTextListener listener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            // newText is text entered by user to SearchView
            Toast.makeText(getApplicationContext(), newText, Toast.LENGTH_SHORT).show();
            return false;
        }
    };







}
