package rajmaterialdemo.com.rajmaterialdemo;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.Contacts.People;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.telephony.SmsManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;

/**
 * Clase de utilidades
 */
public class Utilities {


    public static int pixelsToDpi(Resources res, int pixels) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) pixels, res.getDisplayMetrics());
    }


    public static void setBadgeCount(Context context, LayerDrawable icon, int count) {

        BadgeDrawable badge;

        // Reusar drawable
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }

    public static Boolean emailValidation(String email) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (email.matches(emailPattern) && email.length() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static int getDeviceHeight(Activity activity) {

        ViewGroup.LayoutParams params;
        WindowManager w;
        Display d;
        DisplayMetrics metrics;

        w = activity.getWindowManager();
        d = w.getDefaultDisplay();
        metrics = new DisplayMetrics();
        d.getMetrics(metrics);

//        Log.i("height", "height is : " + d.getHeight());
//        Log.i("width","width is : "+d.getWidth());

        return d.getHeight();


    }

    public static int getDeviceWidth(Activity activity) {

        ViewGroup.LayoutParams params;
        WindowManager w;
        Display d;
        DisplayMetrics metrics;

        w = activity.getWindowManager();
        d = w.getDefaultDisplay();

        metrics = new DisplayMetrics();
        d.getMetrics(metrics);

//        params = viewHolder.exp_cat_rl.getLayoutParams();
//        params.height = d.getHeight() - d.getHeight() / 4;

//        Log.i("height", "height is : " + d.getHeight());
//        Log.i("width","width is : "+d.getWidth());

        return d.getWidth();

    }

    public static int getDeviceWidthOneThird(Activity activity) {

        ViewGroup.LayoutParams params;
        WindowManager w;
        Display d;
        DisplayMetrics metrics;

        w = activity.getWindowManager();
        d = w.getDefaultDisplay();

        metrics = new DisplayMetrics();
        d.getMetrics(metrics);

//        params = viewHolder.exp_cat_rl.getLayoutParams();
//        params.height = d.getHeight() - d.getHeight() / 4;

//        Log.i("height", "height is : " + d.getHeight());
//        Log.i("width","width is : "+d.getWidth());

        return d.getWidth() / 3;

    }

    public static void setLayoutWidthSquare(View v, Activity activity) {

        ViewGroup.LayoutParams params;


        params = v.getLayoutParams();
        params.width = getDeviceWidth(activity);
        params.height = getDeviceWidth(activity);


    }

    public static void setLayoutWidth(View v, Activity activity) {

        ViewGroup.LayoutParams params;


        params = v.getLayoutParams();
        params.width = getDeviceWidth(activity) - 50;


    }

    public static void setLayoutHeight(View v, Activity activity) {

        ViewGroup.LayoutParams params;


        params = v.getLayoutParams();
        params.height = getDeviceHeight(activity);


    }

    public static void setProductViewHeight(View v, Activity activity) {

        ViewGroup.LayoutParams params;


        params = v.getLayoutParams();
        params.height = getDeviceHeight(activity) / 4;


    }

    /*
     * getBitmapFromURLs
	 */
    public static Bitmap getBitmapFromURLs(String src) {
        try {
            URL url = new URL(src.replace(" ", "%20"));
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = null;
            myBitmap = BitmapFactory.decodeStream(input);

            Bitmap resizedbitmap1 = null;

            return myBitmap;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void closeKeyboard(Activity activity, final EditText ed) {

        Log.e("Keyboard", "close");
        activity.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                        | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        InputMethodManager in = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(ed.getWindowToken(), 0);


    }

    public static void showKeyboard(final Activity activity, final EditText ed) {
        // open keyboard
        // Toast.makeText(con, "open keyboard", Toast.LENGTH_SHORT).show();
        Log.e("Keyboard", "show");
        activity.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE
                        | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        activity.findViewById(ed.getId()).postDelayed(new Runnable() {
            @Override
            public void run() {
                ed.requestFocus();
                InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(activity.INPUT_METHOD_SERVICE);
                inputMethodManager.showSoftInput(ed,
                        InputMethodManager.SHOW_IMPLICIT);
            }
        }, 100);


    }


    public static Bitmap getBitmapRotatedNinety(String path) {//you can provide file path here
        int orientation;
        try {
            if (path == null) {
                return null;
            }
            // decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            // Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE = 70;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 0;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE
                        || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale++;
            }
            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            Bitmap bm = BitmapFactory.decodeFile(path, o2);
            Bitmap bitmap = bm;

            ExifInterface exif = new ExifInterface(path);

            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

            Log.e("ExifInteface .........", "rotation =" + orientation);

            //exif.setAttribute(ExifInterface.ORIENTATION_ROTATE_90, 90);

            Log.e("orientation", "" + orientation);
            Matrix m = new Matrix();

            m.postRotate(90);
            Log.e("in orientation", "" + orientation);
            bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), m, true);
            return bitmap;


        } catch (Exception e) {
            return null;
        }
    }

    public static void sendSMS1(final Activity activity, String phone_number, String message) {
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.putExtra("sms_body", "default content");
        sendIntent.setType("vnd.android-dir/mms-sms");
        activity.startActivity(sendIntent);
    }

    public static void callViaLocalNumber(final Activity act, String phone_number) {


        try {
            Intent intent = new Intent(Intent.ACTION_CALL);
//	intent.putExtra("com.android.phone.extra.slot", 0);
            intent.putExtra("simSlot", 0);
            intent.setData(Uri.parse("tel:" + phone_number));
            act.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendEmail(Activity act, String[] to, String[] cc,
                                 String subject, String extra_text) {
        Log.i("Send email", "");

        String[] TO = to;
        String[] CC = cc;
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");

        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, extra_text);

        try {
            act.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            act.finish();
            Log.i("Sent Email", "Finished sending email...");
        } catch (android.content.ActivityNotFoundException ex) {

        }
    }

    public static void sendSMSDirectly(final Activity activity, String toPhoneNumber, String smsMessage) {
//        String toPhoneNumber = toPhoneNumberET.getText().toString();
//        String smsMessage = smsMessageET.getText().toString();
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(toPhoneNumber, null, smsMessage, null, null);
            Toast.makeText(activity, "SMS sent.",
                    Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(activity,
                    "Sending SMS failed.",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public static void sendSMSViaIntent( final Activity activity, String toPhoneNumber, String smsMessage)
    {
        // The number on which you want to send SMS
        activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", toPhoneNumber, null)));
    }

    private void addContact(Activity act, String name, String phone) {
        ContentValues values = new ContentValues();
        values.put(People.NUMBER, phone);
        values.put(People.TYPE, Phone.TYPE_CUSTOM);
        values.put(People.LABEL, name);
        values.put(People.NAME, name);
        Uri dataUri = act.getContentResolver().insert(People.CONTENT_URI, values);
        Uri updateUri = Uri.withAppendedPath(dataUri, People.Phones.CONTENT_DIRECTORY);
        values.clear();
        values.put(People.Phones.TYPE, People.TYPE_MOBILE);
        values.put(People.NUMBER, phone);
        updateUri = act.getContentResolver().insert(updateUri, values);
    }


    // calendar time

    public static int getCurrentSeconds(){
        Calendar c = Calendar.getInstance();
        int seconds = c.get(Calendar.SECOND);
        return seconds;
    }


}