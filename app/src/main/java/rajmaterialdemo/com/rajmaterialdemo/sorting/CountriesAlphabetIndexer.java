package rajmaterialdemo.com.rajmaterialdemo.sorting;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AlphabetIndexer;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import java.util.List;

import rajmaterialdemo.com.rajmaterialdemo.R;
import rajmaterialdemo.com.rajmaterialdemo.db.RajDB;
import rajmaterialdemo.com.rajmaterialdemo.utils.L;

public class CountriesAlphabetIndexer extends AppCompatActivity {

    private static final String TAG = "CountriesAlphabetIndexer";

    private AlphabetIndexer indexer;

    String[] country_dialing_codes = {"0", "93", "355", "213", "1", "376", "244",
            "1", "672", "1", "54", "374", "297", "247", "61", "43", "994", "1",
            "973", "880", "1", "375", "32", "501", "229", "1", "975", "591",
            "387", "267", "55", "1", "673", "359", "226", "257", "855", "237",
            "1", "238", "1", "236", "235", "56", "86", "61", "61", "57", "269",
            "242", "682", "506", "385", "53", "357", "420", "243", "45", "246",
            "253", "1", "1", "670", "593", "20", "503", "240", "291", "372",
            "251", "500", "298", "679", "358", "33", "594", "689", "241",
            "220", "995", "49", "233", "350", "30", "299", "1", "590", "1",
            "502", "224", "245", "592", "509", "504", "852", "36", "354", "91",
            "62", "98", "964", "353", "972", "39", "225", "1", "81", "962",
            "7", "254", "686", "381", "965", "996", "856", "371", "961", "266",
            "231", "218", "423", "370", "352", "853", "389", "261", "265",
            "60", "960", "223", "356", "692", "596", "222", "230", "262", "52",
            "691", "373", "377", "976", "382", "1", "212", "258", "95", "264",
            "674", "977", "31", "599", "687", "64", "505", "227", "234", "683",
            "672", "850", "1", "47", "968", "92", "680", "970", "507", "675",
            "595", "51", "63", "48", "351", "1", "974", "262", "40", "7",
            "250", "290", "1", "1", "508", "1", "685", "378", "239", "966",
            "221", "381", "248", "232", "65", "421", "386", "677", "252", "27",
            "82", "34", "94", "249", "597", "268", "46", "41", "963", "886",
            "992", "255", "66", "228", "690", "676", "1", "216", "90", "993",
            "1", "688", "1", "256", "380", "971", "44", "1", "598", "998",
            "678", "39", "58", "84", "681", "967", "260", "263",

    };

    String[] countries = {"Select Country", "Afghanistan", "Albania", "Algeria",
            "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica",
            "Antigua And Barbuda", "Argentina", "Armenia", "Aruba",
            "Ascension Island", "Australia", "Austria", "Azerbaijan",
            "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus",
            "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia",
            "Bosnia And Herzegovina", "Botswana", "Brazil",
            "British Virgin Island", "Brunei Darussalam", "Bulgaria",
            "Burkina Fasso", "Burundi", "Cambodia", "Cameroon", "Canada",
            "Cape Verde", "Cayman Island", "Central African Republic", "Chad",
            "Chile", "China", "Christmas Island", "Cocos (Keepling) Island",
            "Colombia", "Comoros", "Congo", "Cook Island", "Costa Rica",
            "Croatia", "Cuba", "Cyprus", "Czech Republic",
            "Democratic Republic of Congo", "Denmark", "Diego Garcia",
            "Djibouti", "Dominica", "Dominican Republic", "East Timor",
            "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea",
            "Estonia", "Ethiopia", "Falkland Islands (Malvinas)",
            "Faroe Islands", "Fiji", "Finland", "France", "French Guiana",
            "French Polynesia", "Gabon", "Gambia", "Georgia", "Germany",
            "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada",
            "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau",
            "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland",
            "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy",
            "Ivory Coast", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya",
            "Kiribati", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia",
            "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein",
            "Lithuania", "Luxembourg", "Macao", "Macedonia", "Madagascar",
            "Malawi", "Malaysia", "Maldives", "Mali", "Malta",
            "Marshall Island", "Martinique", "Mauritania", "Mauritius",
            "Mayotte", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia",
            "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar",
            "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles",
            "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria",
            "Niue", "Norfolk Island", "North Korea", "Northern Mariana Island",
            "Norway", "Oman", "Pakistan", "Palau", "Palestine", "Panama",
            "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland",
            "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania",
            "Russian Federation", "Rwanda", "Saint Helena",
            "Saint Kitts And Navis", "Saint Lucia",
            "Saint Peirre And Miquelon", "Saint Vincent And The Grenadines",
            "Samoa", "San Marino", "Sao Tome And Principe", "Saudi Arabia",
            "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore",
            "Slovakia", "Slovenia", "Solomon Island", "Somalia",
            "South Africa", "South Korea", "Spain", "Sri Lanka", "Sudan",
            "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria",
            "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tokelau",
            "Tonga", "Trinidad And Tobago", "Tunisia", "Turkey",
            "Turkmenistan", "Turks And Caicos", "Tuvalu", "U.S Virgin Island",
            "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom",
            "United States", "Uruguay", "Uzbekistan", "Vanuatu",
            "Vatican City", "Venezuela", "Vietnam", "Wallis And Futuna",
            "Yemen", "Zambia", "Zimbabwe" };


    RajDB db;

    ListView countries_lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countries_alphabet_indexer);

        db = new RajDB(CountriesAlphabetIndexer.this);

        countries_lv = (ListView) findViewById(R.id.countries_lv);

        for (int i = 0; i < countries.length; i++) {

            db.insertCountryRecord(""+i+1,countries[i],country_dialing_codes[i],"false");
        }



//        indexer = new AlphabetIndexer(searchCursor, Compatibility.getCursorDisplayNameColumnIndex(searchCursor), " ABCDEFGHIJKLMNOPQRSTUVWXYZ");



    }






    class CountriesListAdapter extends BaseAdapter implements SectionIndexer {
        private int margin;
        private Bitmap bitmapUnknown;
        private List<CountryItem> clist;
        private Cursor cursor;

        CountriesListAdapter(List<CountryItem> country_list, Cursor c) {
            clist = country_list;
            cursor = c;

//            margin = LinphoneUtils.pixelsToDpi(LinphoneActivity.instance().getResources(), 10);
//            bitmapUnknown = BitmapFactory.decodeResource(LinphoneActivity.instance().getResources(), R.drawable.unknown_small);
        }

        public int getCount() {
            return cursor.getCount();
        }

        public Object getItem(int position) {
            if (clist == null || position >= clist.size()) {
                return 0;
            } else {
                return clist.get(position);
            }
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = null;
//            Contact contact = null;
//            do {
//                contact = (Contact) getItem(position);
//            } while (contact == null);


            CountryItem citem = null;
            do {
                citem = (CountryItem) getItem(position);
            } while (citem == null);



            if (convertView != null) {
                view = convertView;
            } else {
                view = mInflater.inflate(R.layout.item_country, parent, false);
            }



            TextView name = (TextView) view.findViewById(R.id.item_country_name_tv);
            name.setText(citem.getCountry_name());

            TextView id = (TextView) view.findViewById(R.id.item_country_id_tv);
            id.setText(citem.getCountry_id());

//            TextView separator = (TextView) view.findViewById(R.id.separator);
//            LinearLayout layout = (LinearLayout) view.findViewById(R.id.layout);
//            if (getPositionForSection(getSectionForPosition(position)) != position) {
//                separator.setVisibility(View.GONE);
//                layout.setPadding(0, margin, 0, margin);
//            } else {
//                separator.setVisibility(View.VISIBLE);
//                separator.setText(String.valueOf(citem.getCountry_name().charAt(0)));
//                layout.setPadding(0, 0, 0, margin);
//            }

//            ImageView icon = (ImageView) view.findViewById(R.id.icon);
//            if (contact.getPhoto() != null) {
//                icon.setImageBitmap(contact.getPhoto());
//            } else if (contact.getPhotoUri() != null) {
//                icon.setImageURI(contact.getPhotoUri());
//            } else {
//                icon.setImageBitmap(bitmapUnknown);
//            }

//            ImageView friendStatus = (ImageView) view.findViewById(R.id.friendStatus);
//            LinphoneFriend[] friends = LinphoneManager.getLc().getFriendList();
//            if (!ContactsManager.getInstance().isContactPresenceDisabled() && friends != null) {
//                friendStatus.setVisibility(View.VISIBLE);
//                PresenceActivityType presenceActivity = friends[0].getPresenceModel().getActivity().getType();
//                if (presenceActivity == PresenceActivityType.Online) {
//                    friendStatus.setImageResource(R.drawable.led_connected);
//                } else if (presenceActivity == PresenceActivityType.Busy) {
//                    friendStatus.setImageResource(R.drawable.led_error);
//                } else if (presenceActivity == PresenceActivityType.Away) {
//                    friendStatus.setImageResource(R.drawable.led_inprogress);
//                } else if (presenceActivity == PresenceActivityType.Offline) {
//                    friendStatus.setImageResource(R.drawable.led_disconnected);
//                } else {
//                    friendStatus.setImageResource(R.drawable.call_quality_indicator_0);
//                }
//            }

            return view;
        }



        @Override
        public int getPositionForSection(int section) {
            L.d(TAG, "getPositionForSection() called with: " + "section = [" + section + "]");
            return indexer.getPositionForSection(section);
        }

        @Override
        public int getSectionForPosition(int position) {
            L.d(TAG, "getSectionForPosition() called with: " + "position = [" + position + "]");
            return indexer.getSectionForPosition(position);
        }

        @Override
        public Object[] getSections() {
            return indexer.getSections();
        }
    }

}
