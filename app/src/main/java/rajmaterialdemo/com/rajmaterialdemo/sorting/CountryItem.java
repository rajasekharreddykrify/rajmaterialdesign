package rajmaterialdemo.com.rajmaterialdemo.sorting;

/**
 * Created by raj on 19/3/16.
 */
public class CountryItem {

    private String country_name,country_id;
    boolean selected;

    public CountryItem() {
    }

    public CountryItem(String country_name, String country_id, boolean selected) {
        this.country_name = country_name;
        this.country_id = country_id;
        this.selected = selected;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
