package rajmaterialdemo.com.rajmaterialdemo.services;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;

import rajmaterialdemo.com.rajmaterialdemo.utils.L;
import rajmaterialdemo.com.rajmaterialdemo.utils.SessionManager;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class NotificationBlockUnblockService extends NotificationListenerService {
    //    private String TAG = this.getClass().getSimpleName();
    private static final String TAG = "NotificationBlockUnblockService";

    public static final String NOT_TAG = "rajmaterialdemo.com.rajmaterialdemo.NOTIFICATION_LISTENER";
    public static final String NOT_POSTED = "POSTED";
    public static final String NOT_REMOVED = "REMOVED";
    public static final String NOT_EVENT_KEY = "not_key";

    SessionManager sessmng;
    public static boolean isNotificationAccessEnabled = false;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        L.i(TAG, "******ImageBlurDownload  onNotificationPosted");
        Intent i = new Intent(NOT_TAG);
        i.putExtra(NOT_EVENT_KEY, NOT_POSTED);
        sendBroadcast(i);

       /* sessmng = new SessionManager(getApplicationContext());
        L.i(TAG, "Notification posted");
        L.i(TAG, "ID :" + sbn.getId() + "t" + sbn.getNotification().tickerText + "t" + sbn.getPackageName());
        L.i(TAG, "getTag :" + sbn.getTag());

        //cancelAllNotifications();

        String packname = sbn.getPackageName();

        if(sessmng.getStoreData(packname).equals("1")){

            cancelNotification(sbn.getPackageName(), sbn.getTag(), sbn.getId());
            if (Build.VERSION.SDK_INT < 21) {
                cancelNotification(sbn.getPackageName(), sbn.getTag(), sbn.getId());
            }
            else {
                cancelNotification(sbn.getKey());
            }
        }*/


    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        L.i(TAG, "Notification Removed");
        L.i(TAG, "ID :" + sbn.getId() + "t" + sbn.getNotification().tickerText + "t" + sbn.getPackageName());

        L.i(TAG, "****** ImageBlurDownload onNotificationRemoved");
        Intent i = new Intent(NOT_TAG);
        i.putExtra(NOT_EVENT_KEY, NOT_REMOVED);
        sendBroadcast(i);

    }

    @Override
    public IBinder onBind(Intent mIntent) {
        IBinder mIBinder = super.onBind(mIntent);
        isNotificationAccessEnabled = true;
        return mIBinder;
    }

    @Override
    public boolean onUnbind(Intent mIntent) {
        boolean mOnUnbind = super.onUnbind(mIntent);
        isNotificationAccessEnabled = false;
        return mOnUnbind;
    }

}