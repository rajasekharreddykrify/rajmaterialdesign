package com.android.internal.telephony;

/**
 * Created by raj on 24/2/16.
 */



public interface ITelephony {

    boolean endCall();

    void answerRingingCall();

    void silenceRinger();
}